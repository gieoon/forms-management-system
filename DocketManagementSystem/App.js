/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/*
Form Management System
DAY -> JOBS
JOB -> FORMS
      - Docket Form
      - Hazard Form
      - Sign In Form
      - Trucker Form
      - Tools Form
LEAVE REQUESTS
TIMESHEETS

EmployeeObj
FormObj
JobObj


Tools Locations

Employee view, 
Manager view
*/


//Note down the required forms for each person.
//Note down the interesting aspects of each person.
require('react-native-gesture-handler'); // Add this to the beginning of App.js
import { 
  createStackNavigator, 
  createAppContainer,
  createBottomTabNavigator,
  //TabNavigator,
  //TabBarBottom 
} from "react-navigation";
import React, {Component} from 'react';
import {StyleSheet, TouchableHighlight, Text, View, ScrollView, RefreshControl, TouchableOpacity} from 'react-native';


import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
//import Icon from 'react-native-vector-icons/dist/FontAwesome';
//Local
import HomeScreen from './Shared/Screens/homeScreen.js'
import FormsScreen from './Shared/Screens/formsScreen.js'
import PersonScreen from './Shared/Screens/personScreen.js'
import FormDetail from './Shared/Screens/formDetail.js'
import ActivityScreen from './Shared/Screens/activityScreen.js';
import ActivityDetail from './Shared/Screens/activityDetail.js';


global.loggedInUser = {}

/* Thematic colors */

// const AppNavigator = createStackNavigator({
//   // Home: {screen: HomeScreen},
//   // Forms: {
//   //   screen: FormsScreen
//   // },
//   // Person: {
//   //   screen: PersonScreen
//   // },
//   // FormDetail: {
//   //   screen: FormDetail
//   // },
// });

import firebase from 'react-native-firebase';
let db = firebase.firestore()
let settings = db.settings
settings.areTimestampsInSnapshotsEnabled = true
db.settings = settings

const FormsStack = createStackNavigator({
  Forms: FormsScreen,
  'New Form': FormDetail,
},
{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      var color = focused ? 'blue' : 'gray';
      //var iconName = `ios-list${focused ? '-box' : ''}`;
      var iconName = 'ios-add';
      return <Ionicons name={iconName} size={35} color={color} />
    },
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
    },
  })
});

const PersonStack = createStackNavigator({
  'Your Assignments': PersonScreen
},
{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      var color = focused ? 'blue' : 'gray';
      //var iconName = `ios-${focused ? 'contact' : 'person'}`;
      return <Ionicons name="ios-contact" size={30} color={color} />
    },
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
    },
  })
})

const ActivityStack = createStackNavigator({
  'Completed': ActivityScreen,
  Activity: ActivityDetail,
}, 
{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      var iconName = `ios-checkbox${focused ? '' : '-outline'}`;
      //var iconName = `ios-square${focused ? '' : '-outline'}`;
      //var iconName = 'ios-folder-open' //focused ? 'ios-folder-open' : 'ios-folder';
      var color = focused ? 'blue' : 'gray'; 
      return <Ionicons name={iconName} size={30} color={color} />
    },
    
    tabBarOptions: {
      activeTintColor: 'blue',
      inactiveTintColor: 'gray',
    },
  })
}
)

const MainTabNavigator = createBottomTabNavigator(
  {
    'Completed': ActivityStack,
    'New Form': FormsStack,
    'Your Assignments': PersonStack
  },
//   {
//   navigationOptions: ({ navigation }) => ({
//     tabBarIcon: ({ focused, tintColor }) => {
//       const { routeName } = navigation.state;
//       console.log('routeName: ', routeName);
//       let iconName;
//       if (routeName === 'Person') {
//         iconName = `ios-information-circle${focused ? '' : '-outline'}`;
//       } else if (routeName === 'All Forms') {
//         iconName = `ios-options${focused ? '' : '-outline'}`;
//       } else if (routeName === 'All Activity')

//       // You can return any component that you like here! We usually use an
//       // icon component from react-native-vector-icons
//       return <Ionicons name={iconName} size={25} color={tintColor} />;
//     },
//   }),
//   tabBarOptions: {
//     activeTintColor: 'tomato',
//     inactiveTintColor: 'gray',
//   },
//   tabBarPosition: 'bottom',
//   animationEnabled: false,
//   swipeEnabled: false,
// }

)

const AppNavigator = createStackNavigator({
  Home: { 
    screen: HomeScreen,
  },
  Forms: MainTabNavigator
}, 
{headerMode: 'none'},
)

//can put stacks within other ones. 
//wow. so, child stacks within other stacks...
//https://reactnavigation.org/docs/en/tab-based-navigation.html

export default createAppContainer(AppNavigator);


    // {
    //   navigationOptions: {
    //     headerTintColor: '#fff',
    //     headerStyle: {
    //       backgroundColor: '#000',
    //     },
    //   },
    // }

/*
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
*/
