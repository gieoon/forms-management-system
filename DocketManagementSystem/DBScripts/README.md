
## Change Scripts

This is an npm repository for altering the DB directly without having to go through UI
It also gives a historical audit to track any past issues.
In conjunction with git, it provides a paper trail.

Change scripts are placed in the ./Scripts folder
Each scripts references main.js which exposes a firestore entry for use by the script.

Scripts do not return data.

### How to write a script

Add the header to the top, this has the standard format
- Date of script
- Who is running the script
- The script's intentions at the top of the file

```
/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/
```

Log the runtime date.
Log the start of the script and the filename that is being run as well 

Create an array of promises and push each action into a Pending Promises
```
const promises = [];
promises.push(/* db query */);
Promise.all(promises).then(() -> {
	console.log(/* Log completion here */);
})
```
Once all promises are complete, log successful completion

### Note
There are no rollbacks, no commits, it's not SQL, so will have to be careful with executions.

To bypass this, the following measures will be implemented: 
- A TestUAT server will be set up and initialized witht he same ChangeScripts, as well 
- Regular backups of the whole database.

