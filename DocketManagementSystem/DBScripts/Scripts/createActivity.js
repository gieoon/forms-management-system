/********************************************************
	DATE: 12/06/2019
	WHO: gieoon
	INTENTION: Initializes a sample Assignment within own branch
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/
promises.push(
		db.collection('Assignments')
			.doc('Form_Data') //also have user edited data, job assignment data, etc. Any historical data is here
			//.collection('3sMIIiOE0cny5zrRfIzY') // Dan Shirley
			.collection('cdj8bggwkGPQgVORuGkC') // Blank user
			.doc('Docket') //Form Title as unique ID
			.set({
				[`${Date.now()}`]: {
					formScope: 'GeneralForms',
				// Form input variables are saved into form data
				// Mapped from variable name to variable value
					formData: {
						'docketWhen': new Date().toDateString(),
						'docketNo': 123456,
						'docketJobName': 'Going to get the milk',
						'docketWhat': 'Dairy, then back to the missus'
					}
				}	
			}, {merge: true})
			.then(()=>{
				console.log("Success");
			})
			.catch((err)=>{
				console.error("Error: ", err);
			})
);


/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
}).then(process.exit);
