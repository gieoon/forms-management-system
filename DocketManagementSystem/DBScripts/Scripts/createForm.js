
/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a Form, have to specify which scope they are in 
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Sites')
		.doc('GREY ST')
		.collection('forms')
		.doc('GREY_ST_Gear_Checklist')
		.set({
			modifiedDate: Date.now(),
			required: true,
			data: [
				{"Label": "Be sure to fill in this checklist everyday when you start work at Grey St"},
				{"Text": "Wearing headgear"},
				{"Checkbox": {
						data: [
							"Yes",
							"No"
						],
						varTitle: "checkboxYesNo"
					}
				},
				{"Text": "Checked all equipment is present"},
				{"Checkbox": {
						data: [
							"All Present",
							"Missing"
						],
						varTitle: "checkboxPresent"
					}
				},
				{"Text": "Signage quality"},
				{"Checkbox": {
						data: [
							"Very good",
							"Mostly good",
							"Not good"
						],
						varTitle: "checkboxGoodness"
					}
				},
				{"Text": "Additional Comments:"},
				{"Input": "comments"},
				{"Button": "Done"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
}).then(process.exit);
