/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a General Form
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('GeneralForms')
		.doc('Daily Docket')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Label": "229 Main Road Lower Moutere, RD 2"},
				{"Label": "Upper Moutere 7175"},
				{"Label": "Phone 03 526 7719"},
				{"Label": "Fax 03 526 7737"},
				{"Spacer": ""},
				{"Text": "Client/contract name:"},
				{"Input": "clientName"},
				{"Text": "Employee name:"},
				{"Input": "employeeName"},
				{"Text": "Phone #:"},
				{"Input": "phoneNumber"},
				{"Text": "Date:"},
				{"DateInput": "docketWhen"},
				{"Text": "Address and/or Email for invoicing:"},
				{"Input": "addressOrEmail"},
				{"Text": "Total Hours at work:"},
				{"Input": "totalHoursAtWork"},
				{
					"List": {
						"listName": "plantList",
						"listData": [
							{"Text": "Plant No."},
							{"Input": "plantNo"},
							{"Text": "Description of work"},
							{"Input": "descriptionOfWork"},
							{"Text": "Start Time"},
							{"Input": "startTime"},
							{"Text": "Finish Time"},
							{"Input": "finishTime"},
							{"Text": "Total Time"},
							{"Input": "totalTime"}
						]
					}
				},
				{
					"List": {
						"listName": "productList",
						"listData": [
							{"Text": "Product"},
							{"Input": "product"},
							{"Text": "Amount"},
							{"Input": "amount"},
							{"Text": "Quarry"},
							{"Input": "quarry"}
						]
					}
				},
				{
					"List": {
						"listName": "plantsHired",
						"listData": [
							{"Text": "Plant hired"},
							{"Input": "plantHired"},
							{"Text": "Date picked up/returned"},
							{"DateInput": "pickupOrReturnDate"},
							{"Text": "Materials picked up"},
							{"Input": "materials"},
							{"Text": "Company hired from"},
							{"Input": "companyHiredFrom"},
							{"Text": "Spoil cart offsite to which dump site"},
							{"Input": "spoilsDumpSite"}
						]
					}
				},
				{"Text": "Who else worked with you today (if not a subdivision) ?"},
				{"Input": "whoElseWorkedOnsite"},
				{"Text": "Comments:"},
				{"Input": "comments"},
				{"Text": "Which machines or plant did you check today?"},
				{"Input": "whichMachinesOrPlant"},
				{"Button": "Save"}
			]
		}).then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log('Error: ', err);
		})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});
