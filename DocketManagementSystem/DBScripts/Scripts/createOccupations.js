/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes Occupations
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Occupations')
		.doc('Driver')
		.collection('forms')
		.doc('Driver_SCAN_IN')
		.set({
				modifiedDate: Date.now(),
				data: [
					{"Button": "Scan In"}
				]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Occupations')
		.doc('Driver')
		.collection('forms')
		.doc('Driver_INCIDENT_REPORT_FORM')
		.set({
				modifiedDate: Date.now(),
				data: [
					{"Text": "Driver Report Incident"},
					{"Button": "Scan In"}
				]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Occupations')
		.doc('Operator')
		.collection('forms')
		.doc('Operator_SCAN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Occupations')
		.doc('Labourer')
		.collection('forms')
		.doc('Labourer_SCAN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Occupations')
		.doc('Labourer')
		.collection('forms')
		.doc('Labourer_INCIDENT_REPORT_FORM')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Text": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Occupations')
		.doc('Office')
		.collection('forms')
		.doc('Office_SCAN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);


/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});
