/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes Scopes
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Scopes')
		.doc('Occupation')
		.set({
			ScopeLevel: 4
		})
		.then(()=>{
			console.log("Success");
		})
		.catch((err) =>{
			console.log("Error setting scope: ", err);
		})
);

promises.push(
	db.collection('Scopes')
		.doc('Site')
		.set({
			ScopeLevel: 3
		})
		.then(()=>{
			console.log("Success");
		})
		.catch((err) =>{
			console.log("Error setting scope: ", err);
		})
);

promises.push(
	db.collection('Scopes')
		.doc('Job')
		.set({
			ScopeLevel: 2
		})
		.then(()=>{
			console.log("Success");
		})
		.catch((err) =>{
			console.log("Error setting scope: ", err);
		})
);

promises.push(
	db.collection('Scopes')
		.doc('Person')
		.set({
			ScopeLevel: 1
		})
		.then(()=>{
			console.log("Success");
		})
		.catch((err) =>{
			console.log("Error setting scope: ", err);
		})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});
