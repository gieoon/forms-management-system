/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Sites')
		.doc('New')
		.collection('forms')
		.doc('New_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Sites')
		.doc('PUKUTUKU')
		.collection('forms')
		.doc('PUKUTUKU_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Sites')
		.doc('SUFFOLK RD')
		.collection('forms')
		.doc('SUFFOLK_RD_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Sites')
		.doc('GREY ST')
		.collection('forms')
		.doc('GREY_ST_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Sites')
		.doc('BIRDHURST_QUEEN_VIC_ST')
		.collection('forms')
		.doc('BIRDHURST_QUEEN_VIC_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

promises.push(
	db.collection('Sites')
		.doc('BIRDHURST_WHAKAWEKA_ST')
		.collection('forms')
		.doc('BIRDHURST_WHAKAWEKA_SIGN_IN')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Button": "Scan In"}
			]
		})
		.then(()=>{
			console.log('Success');
		})
		.catch((err)=>{
			console.log("Error: ", err);
		})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});
