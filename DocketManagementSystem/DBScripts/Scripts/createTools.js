/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Tools')
		.doc('Digger_2.4TON')
		.collection('forms')
		.doc('Digger_Operation_Hazards_&_Risks')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Text": "I hereby comply with..."},
				{"Button": "Agree"}
			]
		})
);

promises.push(
	db.collection('Tools')
		.doc('Digger_2.4TON')
		.set({
			location: {
				longitude: 0,
				latitude: 0
			},
			requirements: ['Heavy Works Licence', 'Medium Works Licence']
		})
);

promises.push(
	db.collection('Tools')
		.doc('Digger_4.8TON')
		.collection('forms')
		.doc('Large_Machine_Operation_Hazards_&_Risks')
		.set({
			modifiedDate: Date.now(),
			data: [
				{"Text": "I hereby comply with..."},
				{"Button": "Agree"}
			]
		})
);

promises.push(
	db.collection('Tools')
	.doc('Digger_4.8TON')
	.set({
		location: {
			longitude: 0,
			latitude: 0
		},
		requirements: ['Massive Works Licence', 'Heavy Works Licence']
	})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});

