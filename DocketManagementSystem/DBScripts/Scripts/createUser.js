/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Creates a single User
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Users')
		.doc()
		.set({
			FirstName: 'Jon',
			LastName: 'Snow',
			UserRole: ['Employee', 'Labourer'],
			Occupation: ['Office'],
			Tool: ['Digger_2.4TON'],
			Site: ['GREY ST'],
			StartDate: '10/02/2018'
		})
		.then(()=>{
			console.log('Success')
		})
		.catch((err) => {
			console.log("Error: ", err)
		})
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
}).then(process.exit);

