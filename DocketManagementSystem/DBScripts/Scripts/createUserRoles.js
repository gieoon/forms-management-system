/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

promises.push(db.collection('UserRoles')
	.doc("Employee_Standard")
	.collection('forms')
	.doc('Standard Employee Sign In Form')
	.set({
		modifiedDate: Date.now(),
		data: [
			{"Button": "Sign In"}
		]
	})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));

promises.push(db.collection('UserRoles')
	.doc("Employee_Standard")
	.collection('forms')
	.doc('Standard Employee Sign Out Form')
	.set({
		modifiedDate: Date.now(),
		data: [
			{"Button": "Sign Out"}
		]
	})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));

promises.push(db.collection('UserRoles')
	.doc("Employee_Standard")
	.set({
		priorityLevel : 2,
	})
);

promises.push(db.collection('UserRoles')
	.doc("Employee_Readonly")
	.set({
		priorityLevel : 2
		})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));

promises.push(db.collection('UserRoles')
	.doc("Manager")
	.set({
		priorityLevel : 3
		})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));

promises.push(db.collection('UserRoles')
	.doc("Manager_Readonly")
	.set({
		priorityLevel : 3
		})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));

promises.push(db.collection('UserRoles')
	.doc("Administrator")
	.set({
		priorityLevel : 4
		})
	.then(function(){
		console.log("Success");
	})
	.catch(function(err){
		console.log("Error executing script: ", err);
	}));



Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});

/*
export const userRole = {
	EMP_STD: new PriorityLevel(2, "Employee_Standard"),
	EMP_READONLY: new PriorityLevel"Employee_Readonly",
	ADMIN: "Administrator",

} 
*/
/* higher priority level is more important */
/*export function PriorityLevel(priorityLevel, stringTitle){
	this.priorityLevel = priorityLevel;
	this.stringTitle = stringTitle;
}
*/
