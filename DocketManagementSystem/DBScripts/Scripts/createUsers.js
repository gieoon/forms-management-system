/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];


promises.push(
	(()=>{
		const ref = db.collection('Users').doc();
		var id = ref.id;
		ref.set({
				FirstName: 'Test',
				LastName: 'Manager',
				UserRole: ['Manager', 'Labourer'],
				Occupation: ['Office'],
				Tool: ['Digger_2.4TON'],
				Site: ['PUKUTUKU'],
				StartDate: '07/02/2018'
			})
			.then(()=> {
				console.log('Successfully created user');
			})
			.catch((err) => {
				console.log('Error executing: ', err);
			})
		db.collection('Users')
			.doc(id)
			.collection('forms')
			.doc("Test_Manager's_SIGN_IN")
			.set({
				modifiedDate: Date.now(),
				data: [
					{"Button": "Scan In"}
				]
			})
	})()
);


promises.push(
	db.collection('Users')
		.doc()
		.set({
			FirstName: 'Test',
			LastName: 'User',
			UserRole: ['Employee_Standard'],
			Occupation: ['Office'],
			Tool: ['Digger_2.4TON'],
			Site: ['PUKUTUKU'],
			StartDate: '23/08/1992'
		})
		.then(()=> {
			console.log('Successfully created user');
		})
		.catch((err) => {
			console.log('Error executing: ', err);
		})
);
promises.push(
	db.collection('Users')
		.doc()
		.set({
			FirstName: 'Dan',
			LastName: 'Shirley',
			UserRole: ['Employee_Standard'],
			Occupation: ['Office'],
			Tool: ['Digger_2.4TON'],
			Site: ['PUKUTUKU'],
			StartDate: '23/08/1992'
		})
		.then(()=> {
			console.log('Successfully created user');
		})
		.catch((err) => {
			console.log('Error executing: ', err);
		})
);

promises.push(
	(()=>{
		const ref = db.collection('Users').doc();
		var id = ref.id;
		ref.set({
				FirstName: '',
				LastName: '',
				UserRole: ['Employee_Standard'],
				Occupation: ['Office'],
				Tool: ['Digger_4.8TON'],
				Site: ['GREY ST'],
				StartDate: '21/09/2008'
			})
			.then(()=> {
				console.log('Successfully created user');
			})
			.catch((err) => {
				console.log('Error executing: ', err);
			})
		db.collection('Users')
			.doc(id)
			.collection('forms')
			.doc("Individual_Signin_Form")
			.set({
				modifiedDate: Date.now(),
				data: [
					{"Text: ": "Only I can see this form"},
					{"Button": "Scan In"}
				]
			})
	})()
	
);

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
});
