/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

promises.push(
	db.collection('Assignments')
		.doc('Job_Assignments')
		.collection('cdj8bggwkGPQgVORuGkC')
		.doc(Date.now().toString())
		.set({
			Status: 'CANCELLED',
			Title: 'Driver & Digger for GREY ST',
			StartDate: 'Mon Jul 1 2019',
			AssignmentEndDate: 'Thur Jul 1 2019',
			Occupations: ['Driver'],
			Sites: ['GREY ST'],
			Tools: ['Digger_2.4TON', 'Digger_4.8TON'],
			UserRoles: ['Employee']
		})	
);

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
}).then(process.exit);
