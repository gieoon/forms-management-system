/********************************************************
	DATE: 08/06/2019
	WHO: gieoon
	INTENTION: Initializes a User's Roles with priorities
*********************************************************/

const db = require('../main.js');
const date = require('../date.js');
console.log("RUNTIME: ", date);
console.log("--- STARTING EXECUTION OF " + __filename + " ---");
const startTime = new Date();

const promises = [];

/****************************************************************/

// Write Code Here

/****************************************************************/

Promise.all(promises).then(() => {
	const endTime = (new Date().getTime() - startTime.getTime()) / 1000;
	console.log("--- EXECUTION COMPLETED SUCCESSFULLY " + __filename + " ---");
	console.log("IN " + endTime.toString() + " SECONDS");
}).then(process.exit);
