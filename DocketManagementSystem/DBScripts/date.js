/*
	Provides the caller with a Date Object to use
*/

function getNow(){
	var now = new Date();
	var dd = String(now.getDate()).padStart(2, '0');
	var mm = String(now.getMonth()).padStart(2, '0');
	var yyyy = now.getFullYear();
	return dd + '/' + mm + '/' + yyyy;
}

module.exports = getNow();