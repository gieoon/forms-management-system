const firebase = require('firebase');
require("firebase/firestore");

var firebaseConfig = {
    apiKey: "AIzaSyCfumiIuDGt2bbKm0RIHI5icPOuDMe6DA8",
    authDomain: "formmanagementsystem-c7f17.firebaseapp.com",
    databaseURL: "https://formmanagementsystem-c7f17.firebaseio.com",
    projectId: "formmanagementsystem-c7f17",
    storageBucket: "formmanagementsystem-c7f17.appspot.com",
    messagingSenderId: "152121265733",
    appId: "1:152121265733:web:78c66e017af2f759"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

var db = firebase.firestore();

//let db = Firestore.firestore()
let settings = db.settings
settings.areTimestampsInSnapshotsEnabled = true
db.settings = settings

module.exports = db;



