import * as ActivityDB from './activity_DB.js';

export async function getActivity(userId){
	//console.log('getting activity')
	return await ActivityDB.getActivityForUser(userId);
}
