import firebase from 'react-native-firebase';
import * as DB_CONSTANTS from '../DB/DB_CONSTANTS.js';

//Query User's activity to get a complete history of what forms they have filled in
export async function getActivityForUser(userId){
	const promises = [];
	const activities = {};
	//console.log(userId)
	promises.push(firebase.firestore() 
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_ASSIGNMENTS)
		.doc('Form_Data')
		.collection(userId)
		.get()
		.then(function(formTypeDocs){
			promises.push(formTypeDocs.forEach(function(doc){
				//console.log('doc.id: ', doc.id);
				const formType = doc.id;
				activities[formType] = doc.data()
			}));
		})
		.catch((err) => {
			console.log("Error: ", err);
		})
	);

	return await Promise.all(promises).then(()=>{
		return activities;	
	});
	//console.log('activities: ', activities);
}

export async function deleteForm(userId, formTitle, timestamp){
	console.log('deleting timestamp of: ', timestamp);
	return await firebase.firestore()
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_ASSIGNMENTS)
		.doc('Form_Data')
		.collection(userId)
		.doc(formTitle)
		.update({
			[`${timestamp}`]: firebase.firestore.FieldValue.delete()
		})
		.then(()=>{
			console.log("Success");
			return true;
		})
		.catch( err =>{
			console.log("Error deleting form: ", err)
			return false;
		});
}

export async function getFormStructureDBUsingScopeAndTitle(formScope, parentPath, formTitle){
	const promises = [];
	var form = {};
	//console.log('parentPath: ', parentPath);
	//console.log('formScope: ', formScope.replace(/[ ]/g,''))
	//console.log('formTitle: ', formTitle);
	if(formScope.replace(/[ ]/g,'') === DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS){
		//console.log('Looking for general form');
		promises.push(firebase.firestore()
			.collection(formScope)
			.doc(formTitle)
			.get('data')
			.then(function(doc){
				if(doc.exists){
					//console.log("Document data: ", doc.data());
					form = doc.data();
					//console.log('Got form: ', form);
				} else {
					console.log("No such document");
				}
			})
			.catch((err) =>{
				console.log("Error retrieving form: ", err);
			})
		);
	}
	else {
		promises.push(
			firebase.firestore()
				.collection(formScope)
				.doc(parentPath)
				.collection('forms')
				.doc(formTitle)
				.get()
				.then(function(doc){
					if(doc.exists){
						//console.log("Document data: ", doc.data());
						form = doc.data();
						//console.log("Got form: ", form);
					} else {
						console.log("No such document");
					}
				})
				.catch((err) => {
					console.log("Error retrieving form: ", err);
				})
		);
	}

	await Promise.race(promises);
	console.log('returning form: ', form);
	return form;
}

//for individual to get their own form types
export async function getActivityByFormType(userId, formType){

}
