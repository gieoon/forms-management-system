
/* DB ROOT LEVEL COLLECTION ENDPOINTS */
export const FIRESTORE_ENDPOINT_COMPANIES = 'Companies';
export const FIRESTORE_ENDPOINT_USERS = 'Users';
export const FIRESTORE_ENDPOINT_JOBS = 'Jobs';
export const FIRESTORE_ENDPOINT_TOOLS = 'Tools';
export const FIRESTORE_ENDPOINT_EXCEPTION_LOG = 'ExceptionLog';
export const FIRESTORE_ENDPOINT_USERROLES = 'UserRoles';
export const FIRESTORE_ENDPOINT_OCCUPATIONS = 'Occupations';
export const FIRESTORE_ENDPOINT_SCOPES = 'Scopes';
export const FIRESTORE_ENDPOINT_SITES = 'Sites';
export const FIRESTORE_ENDPOINT_GENERALFORMS = 'GeneralForms';
export const FIRESTORE_ENDPOINT_ASSIGNMENTS = 'Assignments';

export function getUIDisplay(endpoint){
	switch(endpoint){
		case FIRESTORE_ENDPOINT_COMPANIES: return 'Company';
		case FIRESTORE_ENDPOINT_USERS: return 'User';
		case FIRESTORE_ENDPOINT_GENERALFORMS: return 'General';
		case FIRESTORE_ENDPOINT_USERROLES: return 'User Roles';
		default: return endpoint;
	}
}

