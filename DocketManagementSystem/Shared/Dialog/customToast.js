import React, { Component } from 'react';
import {StyleSheet, Dimensions, Animated, TouchableHighlight, Text, View} from 'react-native';
//https://medium.com/@dabit3/creating-a-custom-toast-module-for-react-native-770fd1c0dcf5
export default class CustomToast extends Component {
	constructor(props){
		super(props);
		this.state = { 
			textToShow: ''
		}
		this.animatedValue = new Animated.Value(40);
		this.showToast = this.showToast.bind(this);
	}

	componentDidMount(){
		//console.log('setting toast: ', this.showToast);
		this.props.setCustomToast(this.showToast);
	}

	showToast(msg){
		this.setState({
			textToShow: msg
		});
		console.log('showing Toast');
		Animated.timing(
			this.animatedValue,
			{
				toValue: 0,
				duration: 750
			}
		).start(this.hideToast())
	}

	hideToast(){
		setTimeout(()=>{
			Animated.timing(
				this.animatedValue,
				{
					toValue: 40,
					duration: 350
				}).start()
		}, 3000)
	}

	render(){
		return(
			<Animated.View 
				style={{ 
					transform: [{ translateY: this.animatedValue}],
					height: 40,
					backgroundColor: 'black',//'#347CEF',//'black',
					position: 'absolute',
					left: 0,
					bottom: 0,
					right: 0,
					justifyContent: 'center'
				}}>
				<Text
					style={{
						marginLeft: 10,
						color: 'white',
						fontSize: 16,
						fontWeight: 'bold',
						textAlign: 'center',
						fontFamily: 'Roboto Light'
					}}>
					{this.state.textToShow}
				</Text>
			</Animated.View>
		);
	}
}
/*

transform [{perspective: number}, {rotate: string}, {rotateX: string}, {rotateY: string}, {rotateZ: string}, {scale: number}, {scaleX: number}, {scaleY: number}, {translateX: number}, {translateY: number}, {skewX: string}, {skewY: string}]

*/
