import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, ScrollView, BackHandler, RefreshControl, TouchableOpacity} from 'react-native';


export default class FullWidthToast extends Component {
	constructor(props){
		super(props);
		this.state = {
			visible: true
		}
	}

	stopShowing(){
		this.setState({
			visible: false
		});
	}

	render(){
		const component = this;
		setTimeout(()=>{
			component.setState({visible: false})
		}, 2000);
		return(
			<View>
			{
				this.state.visible &&
				<View style={styles.bottom}>
					<Text style={styles.text}>Form Saved Successfully</Text>
				</View>
			}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	bottom: {
		flex: 1,
		justifyContent: 'flex-end',
		padding: 5,
		backgroundColor: '#000'
	},
	text: {
		color: '#FFF',
		fontFamily: 'Roboto Light'
	}
})