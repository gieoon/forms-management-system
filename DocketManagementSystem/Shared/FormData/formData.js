
/*
	The data that has been created and stored in each form.
*/

export function FormData(employeeId, dateCompleted){
	this.employeeId = employeeId;
	this.dateCompleted = dateCompleted;
}
