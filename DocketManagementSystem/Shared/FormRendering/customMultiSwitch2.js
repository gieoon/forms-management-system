// import React, { Component } from 'react';
// import {View, Switch, Text} from 'react-native';
// import Ionicons from 'react-native-vector-icons/Ionicons';

// export default class CustomMultiSwitch2 extends Component {
// 	constructor(props){
// 		super(props);
// 		this.state = {
// 			switchValue: false,
// 			text: this.props.text
// 		}
// 		this.toggleSwitch = this.toggleSwitch.bind(this);
// 	}

// 	toggleSwitch = (value) => {
// 		console.log('value: ', value);
// 		//this.props.onStatusChanged(value);
// 		this.setState({
// 			switchValue: value
// 		})
// 	}

// 	render(){
// 		return(
			
// 				<Switch 
// 					style={{
// 						marginTop: 10
// 					}}>	
// 					onValueChange={this.toggleSwitch}
// 					value={this.state.switchValue}/>
// 		);
// 	}
// }
// <View style={{
// 					flex: 1,
// 					justifyContent: 'center',
// 					alignItems: 'center'}}>
// 					<Text>hi</Text>

import React, { Component } from 'react';
import { 
	Animated,
	Dimensions,
	PanResponder,
	View,
	Platform
} from 'react-native';
import Button from './customMultiswitchButton.js';
import styles from './customMultiswitchStyles.js';
const {width} = Dimensions.get('window');
import PropTypes from 'prop-types';

export default class CustomMultiSwitch2 extends Component {
	constructor(props){
		super(props);
		this.state = {
			currentStatus: props.currentStatus,
			isComponentReady: false,
			position: new Animated.Value(0),
			posValue: 0,
			selectedPosition: 0,
			duration: 100,
			mainWidth: width - 30,
			switcherWidth: width / 2.7,
			thresholdDistance: width - 8 - width / 2.4,
			isFormDetail: props.isFormDetail
		};
		this.isParentScrollDisabled = false;
	}

	componentWillMount(){
		this._panResponder = PanResponder.create({
			onStartShouldSetPanResponder: () => true,
			onStartShouldSetPanResponderCapture: () => true,
			onMoveShouldSetPanResponder: () => true,
			onMoveShouldSetPanResponderCapture: () => true,
			onPanResponderGrant: () => {
				if(!this.isParentScrollDisabled){
					this.props.disableScroll(false);
					this.isParentScrollDisabled = true;
				}
			},

			onPanResponderMove: (evt, gestureState)=>{
				if(!this.props.disableSwitch){
					let finalValue = gestureState.dx + this.state.posValue;
					if(finalValue >= 0 && finalValue <= this.state.thresholdDistance)
						this.state.position.setValue(this.state.posValue + gestureState.dx);
				}
			},

			onPanResponserTerminationRequest: () => true,

			onPanResponderRelease: (evt, gestureState) =>{
				if(!this.props.disableSwitch){
					let finalValue = gestureState.dx + this.state.posValue;
					this.isParentScrollDisabled = false;
					this.props.disableScroll(true);
					if(gestureState.dx > 0){
						if(finalValue >= 0 && finalValue <= 140){
							this.leftSelected(false);
						} else if(finalValue > 140 && finalValue <= 280){
							this.rightSelected(false);
						}
					}
				}
			},

			onPanResponderTerminate: () => {}, 
			onShouldBlockNativeResponder: () => {
				return true;
			}
		});
		this.moveInitialState();
	}

	leftSelected = (value) => {
		if(this.props.disableSwitch && value !== 1) return;
		Animated.timing(this.state.position, {
			toValue: Platform.OS === "ios" ? -1 : 0,
			duration: this.state.duration
		}).start();
		setTimeout(() => {
			this.setState({
				posValue: Platform.OS === "ios" ? -1 : 0,
				selectedPosition: 0
			});
		}, 100);
		this.props.onStatusChanged(0);
	}

	rightSelected = (value) => {
		if(this.props.disableSwitch && value !== 1) return;
		Animated.timing(this.state.position, {
			toValue:
				Platform.OS === "ios"
					? this.state.mainWidth - this.state.switcherWidth
					: this.state.mainWidth - this.state.switcherWidth - 2,
			duration: this.state.duration
		}).start();
		setTimeout(()=>{
			this.setState({
				posValue:
					Platform.OS === "ios"
						? this.state.mainWidth - this.state.switcherWidth
						: this.state.mainWidth - this.state.switcherWidth - 2,
					selectedPosition: 2
			});
		}, 100);
		this.props.onStatusChanged(1)
	}

	getStatus = () => {
		return this.state.selectedPosition;
	}

	moveInitialState = () => {
		switch(Number(this.state.currentStatus)){
			case 0:
				this.leftSelected(1);
				break;
			case 1:
				this.rightSelected(1);
				break;
		}
	}

	render(){
		return(
			<View style={styles.container}>
				<Button type="0" onPress={this.leftSelected} isFormDetail={this.state.isFormDetail} button2={true}/>
				<Button type="2" onPress={this.rightSelected} isFormDetail={this.state.isFormDetail} button2={true}/>
				<Animated.View
					{...this._panResponder.panHandlers}
					style={[
						styles.switcher,
						{
							transform: [{ translateX: this.state.position }]
						}
					]}>
					<Button type={this.getStatus().toString()} active={true} isFormDetail={this.state.isFormDetail} button2={true}/>
				</Animated.View>
			</View>
		);
	}
}


CustomMultiSwitch2.propTypes = {
  disableScroll: PropTypes.func,
  onStatusChanged: PropTypes.func
};

CustomMultiSwitch2.defaultProps = {
  disableSwitch: true
};

