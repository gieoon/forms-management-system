import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  PanResponder,
  View,
  Platform
} from "react-native";
import Button from "./customMultiswitchButton.js";
import styles from "./customMultiswitchStyles";
const { width } = Dimensions.get("window");
import PropTypes from "prop-types";

export default class CustomMultiSwitch3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStatus: props.currentStatus,
      isComponentReady: false,
      position: new Animated.Value(0),
      posValue: 0,
      selectedPosition: 0,
      duration: 100,
      mainWidth: width - 30,
      switcherWidth: width / 2.7,
      thresholdDistance: width - 8 - width / 2.4,
      isFormDetail: props.isFormDetail
    };
    this.isParentScrollDisabled = false;
  }

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onMoveShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponderCapture: () => true,

      onPanResponderGrant: () => {
        // disable parent scroll if slider is inside a scrollview
        if (!this.isParentScrollDisabled) {
          this.props.disableScroll(false);
          this.isParentScrollDisabled = true;
        }
      },

      onPanResponderMove: (evt, gestureState) => {
        if (!this.props.disableSwitch) {
          let finalValue = gestureState.dx + this.state.posValue;
          if (finalValue >= 0 && finalValue <= this.state.thresholdDistance)
            this.state.position.setValue(this.state.posValue + gestureState.dx);
        }
      },

      onPanResponderTerminationRequest: () => true,

      onPanResponderRelease: (evt, gestureState) => {
        if (!this.props.disableSwitch) {
          let finalValue = gestureState.dx + this.state.posValue;
          this.isParentScrollDisabled = false;
          this.props.disableScroll(true);
          if (gestureState.dx > 0) {
            if (finalValue >= 0 && finalValue <= 30) {
              this.notStartedSelected(false);
            } else if (finalValue >= 30 && finalValue <= 121) {
              this.inProgressSelected();
            } else if (finalValue >= 121 && finalValue <= 280) {
              if (gestureState.dx > 0) {
                this.completeSelected(false);
              } else {
                this.inProgressSelected(false);
              }
            }
          } else {
            if (finalValue >= 78 && finalValue <= 175) {
              this.inProgressSelected(false);
            } else if (finalValue >= -100 && finalValue <= 78) {
              this.notStartedSelected(false);
            } else {
              this.completeSelected(false);
            }
          }
        }
      },

      onPanResponderTerminate: () => {},
      onShouldBlockNativeResponder: () => {
        // Returns whether this component should block native components from becoming the JS
        // responder. Returns true by default. Is currently only supported on android.
        return true;
      }
    });
    this.moveInitialState();
  }

  notStartedSelected = (value) => {
    if (this.props.disableSwitch && value !== 1) return;
    Animated.timing(this.state.position, {
      toValue: Platform.OS === "ios" ? -2 : 0,
      duration: this.state.duration
    }).start();
    setTimeout(() => {
      this.setState({
        posValue: Platform.OS === "ios" ? -2 : 0,
        selectedPosition: 0
      });
    }, 100);
    this.props.onStatusChanged(0);
  };

  inProgressSelected = (value) => {

    if (this.props.disableSwitch && value !== 1) return;
    Animated.timing(this.state.position, {
      toValue: this.state.mainWidth / 2 - this.state.switcherWidth / 2,
      duration: this.state.duration
    }).start();
    setTimeout(() => {
      this.setState({
        posValue: this.state.mainWidth / 2 - this.state.switcherWidth / 2,
        selectedPosition: 1
      });
    }, 100);
    this.props.onStatusChanged(1);
  };

  completeSelected = (value) => {
    console.log('disabled switch?: ', this.props.disableSwitch)
    if (this.props.disableSwitch && value !== 1) return;
    Animated.timing(this.state.position, {
      toValue:
        Platform.OS === "ios"
          ? this.state.mainWidth - this.state.switcherWidth
          : this.state.mainWidth - this.state.switcherWidth - 2,
      duration: this.state.duration
    }).start();
    setTimeout(() => {
      this.setState({
        posValue:
          Platform.OS === "ios"
            ? this.state.mainWidth - this.state.switcherWidth
            : this.state.mainWidth - this.state.switcherWidth - 2,
        selectedPosition: 2
      });
    }, 100);
    this.props.onStatusChanged(2);
  };

  getStatus = () => {
    return this.state.selectedPosition;
    // switch (this.state.selectedPosition) {
    //   case 0:
    //     return "Open";
    //   case 1:
    //     return "In Progress";
    //   case 2:
    //     return "Complete";
    // }
  };

  moveInitialState = () => {
    switch (Number(this.state.currentStatus)) {
      case 0:
        this.notStartedSelected(1);
        break;
      case 1:
        this.inProgressSelected(1);
        break;
      case 2:
        this.completeSelected(1);
        break;
    }
  };

  render() {
    console.log('isFormDetail: ', this.state.isFormDetail);
    return (
      <View style={styles.container}>
        <Button type="0" onPress={this.notStartedSelected} isFormDetail={this.state.isFormDetail}/>
        <Button type="1" onPress={this.inProgressSelected} isFormDetail={this.state.isFormDetail}/>
        <Button type="2" onPress={this.completeSelected} isFormDetail={this.state.isFormDetail}/>
        <Animated.View
          {...this._panResponder.panHandlers}
          style={[
            styles.switcher,
            {
              transform: [{ translateX: this.state.position }]
            }
          ]}
        >
          <Button type={this.getStatus().toString()} active={true} isFormDetail={this.state.isFormDetail}/>
        </Animated.View>
      </View>
    );
  }
}

CustomMultiSwitch3.propTypes = {
  disableScroll: PropTypes.func,
  onStatusChanged: PropTypes.func
};

CustomMultiSwitch3.defaultProps = {
  disableSwitch: true
};