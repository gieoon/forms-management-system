/* Switch Button Component class
 */
import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import styles from './customMultiswitchStyles';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';

const getIcon = (type, active) => {
    let icn;
    switch (type) {
    case '0':
        icn = active
            ? <Ionicons name='ios-close' size={50} color='red'/>//require('./assets/slider/active/notstarted.png')
            : <Ionicons name='ios-close' size={45} color='#347CEF'/>//require('./assets/slider/inactive/notstarted.png');
        break;
    case '1':
        icn = active
            ? <Ionicons name='ios-remove' size={45} color='#347CEF'/>//require('./assets/slider/active/inprogress.png')
            : <Ionicons name='ios-remove' size={45} color='#347CEF'/>//require('./assets/slider/inactive/inprogress.png');
        break;
    case '2':
        icn = active
            ? <Ionicons name='ios-checkmark' size={50} color='green'/>//require('./assets/slider/active/complete.png')
            : <Ionicons name='ios-checkmark' size={45} color='#347CEF'/>//require('./assets/slider/inactive/complete.png');
        break;
    }
    return icn;
};

const Button = props => {
    return (
        <View>
            <TouchableOpacity
                onPress={props.onPress}
                style={props.button2 ? styles.buttonStyle2 : styles.buttonStyle}
                activeOpacity={props.isFormDetail ? 0.2 : 1}>
            {getIcon(props.type, props.active)}
            {/*<Image source={getIcon(props.type, props.active)} />*/}
            </TouchableOpacity>
                
        </View>
    );
};

Button.propTypes = {
    type: PropTypes.string,
    active: PropTypes.bool,
    onPress: PropTypes.func,
    //isFormDetail: PropTypes.bool
};

Button.defaultProps = {
    active: false
};

export default Button;