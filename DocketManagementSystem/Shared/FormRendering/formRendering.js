//contains the way that database text is rendered in the UI
//shared between forms and form type activities 
import React, {Component} from 'react';
import {StyleSheet, Dimensions, Switch, TouchableHighlight, FlatList, Image, Text, TextInput, View, Button, ScrollView, BackHandler, RefreshControl, TouchableOpacity} from 'react-native';
import DatePicker from 'react-native-datepicker'
import {formStyles, listStyles} from './formStyles.js';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import CustomMultiSwitch3 from './customMultiswitch3.js';
import CustomMultiSwitch2 from './customMultiSwitch2.js';
import { colors } from '../globalColors.style.js';

//component is optional, acitivty does not pass in component because it does not require interaction
//activity will pass in formdata for rendering
export function renderSwitch(dataKey, dataValue, component, formData, listIndex, formIndex, listTitle, isActivity){
		if(listTitle.length > 0){
			// console.log('datakey: ', dataKey);
			// console.log('dataValue: ', dataValue);
			// console.log('formData: ', formData);
			// console.log('component: ', component);
		}
		switch(dataKey){
			case 'Text':
				return <Text key={formIndex}  style={formStyles.text}>
							{dataValue}
					   </Text>;
			case 'Label':
				return <Text key={formIndex}  style={formStyles.label}>{dataValue}</Text>;
			case 'Button': 
				// assume every button is a submit button, at least for MVP
				return isFormDetail && <TouchableHighlight 
						key={formIndex} 
						style={[formStyles.button, {opacity: component.state.signInBtnOpacity}]}
						onPress={() => component._submit()}>
						<Text style={formStyles.btnLabel}>{dataValue}</Text>
					</TouchableHighlight>
			case 'Input':
				return <TextInput 
					key={formIndex} 
					style={formStyles.input}
					editable={isFormDetail}
					value={isFormDetail ? 
							component.state.toSave[dataValue] || '' 
							: listTitle.length > 0 ? 
								formData[listTitle + '-' + dataValue + '-' + listIndex]
								: formData[dataValue]
					}
					onChangeText={(newText) => {
							//dynamic assignment
							const tState = component.state.toSave || {};
							tState[(listTitle.length > 0 ? listTitle + '-' + dataValue + '-' + listIndex : dataValue)] = newText;
							component.setState({toSave: tState});
							//console.log('this.state.toSave: ', this.state.toSave);
						}
					}
					/>

			case 'DateInput':
				return <DatePicker
					key={formIndex} 
					style={formStyles.datePicker}
					date={isFormDetail ? 
							component.state.toSave[dataValue]
							 : listTitle.length > 0 ?
							 formData[listTitle + '-' + dataValue + '-' + listIndex]
							 : formData[dataValue]} //dynamic assignment
					mode="date"
					disabled={!isFormDetail}
					is24Hour={true}
					placeholder="Select a Date"
					format="MM-DD-YYYY"
					confirmBtnText="Confirm"
					cancelBtnText="Cancel"
					customStyles={{
						dateIcon: {
							position: 'absolute',
							left: 0,
							top: 4,
							marginLeft: 0
						},
						dateInput: {
							marginLeft: 60
						}
					}}
					onDateChange={(newDate) => {
						const tState = component.state.toSave || {};
						tState[(listTitle.length > 0 ? listTitle + '-' + dataValue + '-' + listIndex : dataValue)] = newDate;
						component.setState({toSave:tState});
						//console.log('this.state.toSave ', this.state.toSave); 
					}}
					//override styles by creating a custom touchable component
					//TouchableComponent
				/>
			case 'Spacer': 
				return <Text style={formStyles.spacer}></Text>;
			case 'Image': //for user to upload an image
				return <Image style={formStyles.image} />
			case 'Checkbox':
				//dataValue is a list to render

				//console.log('formData: ', formData)
				//console.log('dataValue: ', dataValue);
				const varTitle = dataValue.varTitle;
				const switchData = dataValue.data;

				if(switchData.length > 2){
					return <View style={formStyles.multiswitch}>
						<CustomMultiSwitch3 
							currentStatus={isFormDetail ? 1 : formData[varTitle]}
							disableScroll={value => {
		                        console.log('scrollEnabled', value);
		                        // this.scrollView.setNativeProps({
		                        //     scrollEnabled: value
		                        // });
		                    }}
		                    templateLength={switchData.length}
		                    isFormDetail={isFormDetail}
		                    disableSwitch={!isFormDetail}
							isParentScrollEnabled={true}
							onStatusChanged={text=> {
								if(isFormDetail){
									const tState = component.state.toSave || {};
									tState[(listTitle.length > 0 ? listTitle + '-' + dataValue + '-' + listIndex : varTitle)] = text;
									component.setState({toSave: tState});
								}
							}}/>
					</View>
				} else {
					return <View style={formStyles.multiswitch}>
								<CustomMultiSwitch2 
									currentStatus={isFormDetail ? 0 : formData[varTitle]}
									disableScroll={value => {}}
									isFormDetail={isFormDetail}
									disableSwitch={!isFormDetail}
									isParentScrollEnabled={true}
									onStatusChanged={text => {
										//console.log('status changed: ', text);
										if(isFormDetail){
											const tState = component.state.toSave ||{};
											tState[(listTitle.length > 0 ? listTitle + '-' + dataValue + '-' + listIndex : varTitle)] = text;
											component.setState({toSave: tState});
										}
									}}/>
					</View>
				}
				
			case 'Comment':
				return <Text style={formStyles.comment}>{dataValue}</Text>
			case 'List':
				//console.log('datakey: ', dataKey);
				//console.log('dataValue: ', dataValue);
				//console.log('formData: ', formData);
				const uniqueListKey = dataValue.listName;
				//console.log('uniqueListKey: ', uniqueListKey)
				const listData = dataValue.listData;
				//console.log('listdata: ', listData);
				if(isFormDetail){
					//user in filling in form
					if (component.state.listEntry[uniqueListKey] === undefined) {
						component.state.listEntry[uniqueListKey] = {
							listLength: [0],
							ref: React.createRef()
						}
					}
				} else {
					//user is not filling in form, activity is displaying it
					
					//analyze formData to get the length of the list
					// var biggest = [];
					// Object.keys(formData).map((key) => {
					// 	if((key.replace(/^[a-zA-Z]+-/,'')));
					// });
					// console.log('got biggest: ', biggest);
					// listObj['listLength'] = biggest;
					const set = {};
					Object.keys(formData).map((key)=>{
						if(/-[0-9]+$/.test(key)){ //if it has hyphen followed by numbers at the end
							var indexNumber = key.match(/-[0-9]+$/)[0];
							if(key.includes(uniqueListKey)){
								set[indexNumber] = key; //using a set here to not put the same number in twice
							}
						}
					});
					listLength = Object.keys(set).length > 0 ? Object.keys(set) : [1];
					component[uniqueListKey] = {
						listData: listData,
						listLength: listLength,
						dotIndex: 0
					}
					//console.log('component: ', component);
					//replace(/-[0-9]+$/,'')
					//split on '-' and remove it, to get key, and the index number

					//admin is displaying the form.
					//authentication of the form
				}
				return  <View style={{paddingTop: 25, paddingBottom: 30}}>{/*, flex: 1, flexDirection: 'row'*/}
							{/*<View style={{alignItems: 'center', marginTop: 'auto', marginBottom: 'auto'}}>
								<Ionicons name='ios-arrow-round-back' size={30} color={'black'} />
							</View>*/}
							<Carousel 
								//layout={'default'}
								ref={isFormDetail ? component.state.listEntry[uniqueListKey].ref : null}
								containerCustomStyle={listStyles.parentContainer}
								horizonal={true}
								data={isFormDetail ? 
										component.state.listEntry[uniqueListKey].listLength
										:
										component[uniqueListKey].listLength
								} 
								slideStyle={{flex:1, width: Dimensions.get('window').width * 0.75}}
								itemWidth={Dimensions.get('window').width * 0.75}
	                  			sliderWidth={Dimensions.get('window').width * 0.80}
								renderItem={({item, index, separators}) => (
									<View>{
										//console.log('item: '+ item + ' index: ' + component.state.listEntry[uniqueListKey].ref.current.currentIndex)
									}
										<View style={listStyles.listContainer}>
										{ 
											//render lists recursively
											listData.map((o, i) => (
												renderSwitch(
													Object.keys(o)[0],
													o[Object.keys(o)[0]],
													component,
													formData,
													listIndex=index,
													formIndex=index.toString() + i.toString(), //this must be a unique identifier to set the key
													listTitle=uniqueListKey,
													isFormDetail
												)
											))
										}
										</View>
										{
											isFormDetail &&
											<View style={listStyles.addRemoveContainer}>
												<TouchableHighlight
													onPress={()=>{
														component.removeFromFormList(uniqueListKey);
													}}>
													<Ionicons name='ios-remove' size={30} color='blue' />
												</TouchableHighlight>
												<TouchableHighlight
													onPress={()=>{
														component.addToFormList(uniqueListKey);
														//console.log('ADD pressed: ', component.state.listLengths);
														//renderSwitch(dataKey, dataValue, component, formData)
													}}>
													<Ionicons name='ios-add' size={30} color='blue' />
												</TouchableHighlight>
											</View>
										}
									</View>
								)}

								// ItemSeparatorComponent={() => {
							 //        return (
							 //            <View
							 //                style={{
								//                 height: "100%",
								//                 width: Dimensions.get('window').width * 0.05,
								//                 backgroundColor: "#CED0CE",
							 //                }}
							 //            />
							 //        );
							 //    }}
							     keyExtractor={(item, index) => index.toString()}
							/>
							<Text>{}</Text>
							<Pagination
								dotsLength={isFormDetail ? 
										component.state.listEntry[uniqueListKey].listLength.length
										:
										component[uniqueListKey].listLength.length}
								activeDotIndex={ -1
									//isFormDetail ?
									//	component.state.listEntry[uniqueListKey].dotIndex
									//	:
									//	component[uniqueListKey].dotIndex
								}
								containerStyle={listStyles.paginationContainer}
								dotColor={'rgba(0,0,0, 0.92)'}
								dotStyle={listStyles.paginationDot}
								inactiveDotColor={colors.black}
								inactiveDotOpacity={0.4}
								inactiveDotScale={0.6}
								carouselRef={this.sliderRef}
								tappableDots={!!this.sliderRef}
							/>
							{/*<View style={{marginTop: 'auto', marginBottom: 'auto'}}>
								<Ionicons name='ios-arrow-round-forward' size={30} color={'black'} />
							</View>*/}
					</View>
			default: 
				return '';
		}
	}


// <MultiSwitch 
// 						choiceSize={40}
// 						activeItemStyle={[{color: 'white'}, {color: 'blue'}, {color: 'white'}]}
// 						layout={{vertical: 0, horizontal: -1}}
// 						containerStyles={ _.times(3, () => ({
// 							backgroundColor: 'white',
// 							borderRadius: 20,
// 							borderWidth: 1,
// 							borderColor: 'lightgrey',
// 							justifyContent: 'space-between',
// 							width: 150
// 						}))}
// 						active = {1}>
// 						<Ionicons name='ios-close' size={30} color='blue' />
// 						{/*<Ionicons name='ios-help' size={30} color='blue' />*/}
// 						<Ionicons name='ios-checkmark' size={30} color='blue' />
// 					</MultiSwitch>