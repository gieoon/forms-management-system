import { Dimensions } from 'react-native';

//for inner data for forms
export const formStyles = {
	text: {
		fontSize: 18,
		color: '#000',
		marginTop: 10,
		fontFamily: 'Roboto Light'
	},
	input: {
		fontSize: 20,
		height: 40,
		width: '100%',
		borderColor: '#96A3B7',
		borderWidth: 1,
		textAlign: 'center',
		fontWeight: 'bold',
		marginTop: 5,
		margin: 10,
		fontFamily: 'Roboto Light'
	},
	btnLabel: {
		color:'#fff',
      	textAlign:'center',
      	fontSize: 18
	},
	button: {
    	marginRight:40,
	    marginLeft:40,
	    marginTop:20,
	    paddingTop:10,
	    paddingBottom:10,
	    backgroundColor: '#347CEF',
	    borderRadius:10,
	    borderWidth: 1,
	    borderColor: '#fff',
	    width: '50%'
  	},
  	datePicker: {
  		width: '100%',
  		margin: 15,
  	},
  	multiswitch: {
  		marginTop: 5,
  		marginBottom: 15
  	},
	label: {
		fontSize: 14,
		fontFamily: 'Roboto Light',
		textAlign: 'left',
		color: '#96A3B7'
	},
	image: {
		width: '100%'
	},
	spacer: {
		height: 5,
		padding: 5
	},
	comment: {
		fontSize: 14,
		color: '#96A3B7',
		fontFamily: 'Roboto Light',
		fontStyle:'italic',
	}
}

export const listStyles = {
	addRemoveContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		width: '100%',
		margin: 5,
		marginBottom: 10
	},
	listContainer: {
		alignItems: 'center',
		width: '100%',
		borderColor: '#96A3B7',
		//borderRadius: 10,
		borderWidth: 1,
		padding: 5,
		paddingLeft: 15,
		paddingRight: 15,
		//marginBottom: 40
	},
	parentContainer: {
		flex: 1,
		// backgroundColor: 'red'
	},
	paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    }
}

/*
		borderColor: '#96A3B7',
		borderWidth: 1,
flex: 1,
padding: 5,
		paddingLeft: 20,
		paddingRight: 20
*/
//alignItems: 'center',
//margin: 30
//width: '100%',