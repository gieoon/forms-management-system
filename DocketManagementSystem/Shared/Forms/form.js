
//forms that are created for each job.
//later on, users will be able to customize the forms, but for now we will preset the forms and map them to commands
import FormField from './formField.js';
import React from 'react';
import {View} from 'react-native';

class Form extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			id: this.props.id,
			title: this.props.title,
			dateCreated: this.props.dateCreated,
			formFields: this.props.formFields //mapping of the form data and field types
		};
	}
	render(){
		return(
			<View>
				{
					this.state.formFields.map(formField => (
						<FormField field={formField.field} validation={formField.validation} />
					))
				}
			</View>
		);
	}
}

export function Form(title, formFields){
	

} 




