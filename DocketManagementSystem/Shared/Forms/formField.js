/*
	The type of field that is used
*/

import React, {Component} from 'react';
import {Text, Button, View} from 'react-native';

class FormField extends Component {
	constructor(props){
		super(props);
		this.state = {
			field: this.props.field,
			validation: this.props.validation
		};
	}

	handleValidationType(){

	}

	render(){
		return(
			<View>
			{
				switch(this.state.field){
					case 'text': return <Text />
					case 'button': return <Button />
				}
			}
			</View>
		);
	}
}


export default FormField;

