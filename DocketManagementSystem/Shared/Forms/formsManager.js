import * as FormsDB from './forms_DB.js'; 

export async function launchForms(userId, user){
	//const occupation = FormsDB.getUserAss(user);
	const forms = await FormsDB.getFormsForUser(
		userId, 
		user.UserRole,
		user.Occupation,
		user.Site,
		user.Tool
	).then((forms)=>{
		//console.log("Got forms successfully: ", forms);
		return forms;
		
	}).catch((err) => {
		//console.error("ERROR launchForms(): ", err)
	})
	//console.log('returning forms: ', forms);
	return await forms;
}

