
import firebase from 'react-native-firebase';
import * as DB_CONSTANTS from '../DB/DB_CONSTANTS.js';


//pull data from all available scopes
export async function getFormsForUser(userId, userRoles, occupations, sites, tools){
	const promises = []; 
	const forms = {};
	promises.push(firebase.firestore()
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_USERS)
		.doc(userId)
		//.where("forms","==",true)
		.collection('forms')
		.get()
		.then(function(formsSnapshot){
			promises.push(formsSnapshot.forEach(function(doc){
				//console.log("User Form Data: ", doc.id + ', ' + doc.data());
				if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERS] === undefined){
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERS] = [];
				}
				forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERS].push({
					parentPath: userId,
					formTitle: doc.id,
					formData: doc.data()
				});
			}))
		}).catch(function(err){
			console.log("Error getting document: ", err);
			Promise.resolve();
		}));

	promises.push(firebase.firestore()
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS)
		.get()
		.then(function(formsSnapshot){
			promises.push(formsSnapshot.forEach(function(doc){
				//console.log("General Forms Data: ", doc.id + ', ' + doc.data());
				if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS] === undefined){
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS] = [];
				}
				forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS].push({
					parentPath: DB_CONSTANTS.FIRESTORE_ENDPOINT_GENERALFORMS,
					formTitle: doc.id,
					formData: doc.data()
				});
			}))
		}).catch(function(err){
			console.log("Error getting document: ", err);
			Promise.resolve();
		}));

	for(role of userRoles){
		promises.push(firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_USERROLES)
			.doc(role)
			.collection('forms')
			.get()
			.then(function(formsSnapshot){
				promises.push(formsSnapshot.forEach(function(doc){
					//console.log("UserRole Form Data: ", doc.id + ', ' + doc.data());
					if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERROLES] === undefined){
						forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERROLES] = [];
					}
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_USERROLES].push({
						parentPath: role,
						formTitle: doc.id,
						formData: doc.data()
					});
				}))
			}).catch(function(err){
				console.error("Error getting document: ", err);
				Promise.resolve();
			}));
	}

	for (occupation of occupations){ //change for...in => for...of
		//console.log(occupation)
		promises.push(firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_OCCUPATIONS)
			.doc(occupation)
			.collection('forms')
			.get()
			.then(function(formsSnapshot){
				promises.push(formsSnapshot.forEach(function(doc){
					//console.log("Occupation Form Data: ", doc.id + ', ' + doc.data());
					if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_OCCUPATIONS] === undefined){
						forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_OCCUPATIONS] = [];
					}
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_OCCUPATIONS].push({
						parentPath: occupation,
						formTitle: doc.id,
						formData: doc.data()
					});
				}))
			}).catch(function(err){
				console.log("Error getting document: ", err);
				Promise.resolve();
			}));
	}

	for (var site of sites){
		promises.push(firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_SITES)
			.doc(site)
			.collection('forms')
			.get()
			.then(function(formsSnapshot){
				promises.push(formsSnapshot.forEach(function(doc){
					//console.log("Site Form Data: ", doc.id + ', ' + doc.data());
					if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_SITES] === undefined){
						forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_SITES] = [];
					}
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_SITES].push({
						parentPath: site,
						formTitle: doc.id,
						formData: doc.data()
					});
				}))
			}).catch(function(err){
				console.log("Error getting document: ", err);
				Promise.resolve();
			}));
	}

	// TODO this is for dockets, logging every new job, and truckers logging their times too
	// promises.push(firebase.firestore()
	// 	.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_JOBS)
	// 	.doc(job)
	// 	.get('forms')
	// 	.then(function(doc){
	// 		if(doc.exists){
	// 			console.log("Document Data: ", doc.data());
	// 		} else {
	// 			// No document found
	// 			console.log("No Document found: ");
	// 		}
	// 	}).catch(function(err){
	// 		console.log("Error getting document: ", err);
	// 	}));

	for(var tool of tools){
		//console.log(tool);
		promises.push(firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_TOOLS)
			.doc(tool)
			.collection('forms')
			.get()
			.then(function(formsSnapshot){
				promises.push(formsSnapshot.forEach(function(doc){
					//console.log("User Form Data: ", doc.id + ', ' + doc.data());
					if(forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_TOOLS] === undefined){
						forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_TOOLS] = [];
					}
					forms[DB_CONSTANTS.FIRESTORE_ENDPOINT_TOOLS].push({
						parentPath: tool,
						formTitle: doc.id,
						formData: doc.data()
					});
				}))
			}).catch(function(err){
				console.log("Error getting document: ", err);
				Promise.resolve();
			}));
	}
	//console.log('promises: ', promises)
	await Promise.all(promises);
	//console.log('forms db returning forms: ', forms);
	return await forms;
}

export async function saveFormData(userId, formTitle, formType, parentPath, toSave){
	const promises = [];
	const forms = {};
	console.log('saving form data: ' + userId + ', ' + formTitle + ', ' + formType + ', ' + toSave);
	promises.push(
		firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_ASSIGNMENTS)
			.doc('Form_Data')
			.collection(userId)
			.doc(formTitle)
			.set({
				[`${Date.now()}`]: {
					formScope: formType,
					parentPath: parentPath,
					formData: toSave
				}
			}, { merge: true })
			.then(()=>{
				console.log("Success");
			})
			.catch((err)=>{
				console.log("Error: ", err);
			})
	);
	return await Promise.all(promises);
}

/* This is more of an admin activity and can be ignored for now */
export function updateFormContents(id, title, newData){

}

/*
if(doc.exists){
				console.log("UserRole Data: ", doc.data());
				return {
					userRoleForms: doc.data()
				}
			} else {
				// No document found
				console.log("No UserRole Form found: ");
				Promise.resolve();//return {}
			}

*/


