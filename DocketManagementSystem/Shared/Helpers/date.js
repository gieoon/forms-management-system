

// export function timestampToDate(timestamp){
// 	var now = new Date(timestamp * 1000);
// 	var dd = String(now.getDate()).padStart(2, '0');
// 	var mm = String(now.getMonth()).padStart(2, '0');
// 	var yyyy = now.getFullYear();
// 	return dd + '/' + mm + '/' + yyyy;
// }


export function getCleanDate(timestamp){
	//console.log('got: ', timestamp)
	return new Date(Number(timestamp)).toDateString() + ' ' + new Date(Number(timestamp)).toLocaleTimeString(
			navigator.language, 
			{
				hour: '2-digit', 
				minute:'2-digit'
			});
}

export function getDay(timestamp){
	return new Date(timestamp).toDateString();
}

export function getTime(timestamp){
	return new Date(timestamp).toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'});
}

export function getTimestampFromDateString(dateString){
	var t_dateString = dateString.split(" ");
	var newDate = t_dateString[2] + "/" + t_dateString[1] + "/" + t_dateString[3];
	return new Date(newDate).getTime(); 
}