
/*
Each validation object can have an option for voice input, 
*/
function InputObj(str_text, b_voice, validation_type, b_picture){
	this.text = str_text;
	this.voice = b_voice;
	this.validation = validation_type; //the type of validation to pass through
	this.picture = b_picture;
}

export default InputObj;
