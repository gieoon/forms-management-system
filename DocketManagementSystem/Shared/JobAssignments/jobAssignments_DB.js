import firebase from 'react-native-firebase';
import * as DB_CONSTANTS from '../DB/DB_CONSTANTS.js';
//import { getTimestampFromDateString } from '../Helpers/date.js';

export async function getJobAssignmentForUser(userId){
	const promises = [];
	const assignments = [];
	promises.push(
		firebase.firestore()
			.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_ASSIGNMENTS)
			.doc('Job_Assignments')
			.collection(userId)
			.get()
			.then((timestamps) => {
				timestamps.forEach(doc =>{
					console.log(doc.data());
					assignments.push({
						'Status': doc.get('Status'),
						'timestamp': doc.id,
						'Title': doc.get('Title'),
						'StartDate': doc.get('StartDate'),
						'AssignmentEndDate': doc.get('AssignmentEndDate'),
						'Occupations': doc.get('Occupations'),
						'Sites': doc.get('Sites'),
						'Tools': doc.get('Tools'),
						'UserRoles': doc.get('UserRoles')
					})
				})
			})
	);

	await Promise.all(promises);
	return assignments;
}
