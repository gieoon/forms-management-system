/*
	Logs Exceptions to the endpoint
*/
import firebase from 'react-native-firebase';
import * as DB_CONSTANTS from '../DB/DB_CONSTANTS.js';

export function logException(msg){
	firebase.firestore()
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_EXCEPTION_LOG)
		.doc(Date.now().toString())
		.set({
			errMsg: msg
		});
}