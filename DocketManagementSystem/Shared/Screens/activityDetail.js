import React, {Component} from 'react';
import {StyleSheet, Alert, TouchableHighlight, Text, View, ScrollView, RefreshControl, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
//locals
import { getFormStructureDBUsingScopeAndTitle, deleteForm } from '../Activity/activity_DB.js';
import {renderSwitch} from '../FormRendering/formRendering.js';

//reproduce formdetail and render out the form
//create an edit button to be able to edit the existing forms.
export default class ActivityDetail extends Component {
	static navigationOptions = () => ({
    	headerTitle: 'Details'
	});
	constructor(props){
		super(props);
		this.state = {
			activity: this.props.navigation.getParam('activity'),
			formStructure: {},
			formListDetails: {}
		}
		this._deleteForm = this._deleteForm.bind(this);
	}

	componentDidMount(){
		console.log('activity: ', this.state.activity);
		this._fetchFormStructure(
			this.state.activity.formScope,
			this.state.activity.parentPath,
			this.state.activity.formName
		);
	}

	//need to retrieve the form structure to see what it actually looks like to render
	async _fetchFormStructure(formScope, parentPath, formName){
		const formStructure = await getFormStructureDBUsingScopeAndTitle(formScope, parentPath, formName);
		this.setState({
			formStructure: formStructure
		});
		//console.log('form structure: ', formStructure);
	}

	_showAlert(){
		Alert.alert(
			'Are you sure?',
			"All of the data in this form will be permanently erased",
			[
				{
					text: 'Cancel',
					onPress: () => console.log('cancel pressed'),
					style: 'cancel'
				},
				{
					text: 'OK',
					onPress: () => this._deleteForm(),
				}
			],
			{ cancelable: false },
		);
	}

	async _deleteForm(){
			const component = this;
			const isSuccess = await deleteForm(
				global.loggedInUser.userId, 
				this.state.activity.formName,
				this.state.activity.timestamp);
			if(isSuccess){
				setTimeout(()=>{
					component.props.navigation.getParam('refresh')()
				}, 1000);
				this.props.navigation.goBack(null);
				component.props.navigation.getParam('showDeletedToast')();
			} else {
				console.log('Error deleting form');
			}
	}

	render(){
		const component = this;
		//console.log('formData: ', component.state.activity.formData);
		return(
			<View style={styles.container}>
				<Text style={styles.date}></Text>
				<View style={styles.header}>
					<Ionicons name='ios-close' size={35} color='blue'onPress={()=>this._showAlert()}/>
				</View>
				<ScrollView contentContainerStyle={styles.scrollView}>
				{
					(component.state.formStructure.data || []).map((property, index) => (
						
						<View key={index} style={styles.formContainer}>
						{
							renderSwitch(
								Object.keys(property)[0], 
								property[Object.keys(property)[0]], 
								component.state.formListDetails, //activity does not pass in a component
								component.state.activity.formData, //form template to render on //will use less memory to just pass the equal one in
								listIndex=0,
								formIndex=index+1,
								listTitle='',
								isFormDetail=false
							)
						}
						</View>
					))
				}
				</ScrollView>
			</View>
		);
	}
}
/*
<View key={index} style={styles.rowView}>
	<Text style={styles.detail}>{this.state.activity.formData[property]}</Text>
</View>
*/

const styles = StyleSheet.create({
	header: {
		paddingTop: 10,
		//backgroundColor: '#347CEF',
		width: '100%',
		paddingRight: 20,
		paddingBottom: 5,
		alignItems: 'flex-end',
		borderBottomColor: '#96A3B7',
		borderBottomWidth: 1,
	},
	container: {
		flex: 1,
	},
	formContainer: {
		marginTop: 10,
		alignItems: 'center',
		width: '100%',
		flex: 1
	},
	scrollView: {
        backgroundColor: 'transparent',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom: 40,
        alignItems: 'center',
    },
	rowView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	detail: {
		fontSize: 18
	},
	date: {
		fontSize: 16,
		fontFamily: 'Roboto Light'
	}
})

