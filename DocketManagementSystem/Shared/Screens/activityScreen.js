import React, {Component} from 'react';
import {StyleSheet, TouchableHighlight, Text, View, ScrollView, RefreshControl, TouchableOpacity} from 'react-native';

import * as ActivityManager from '../Activity/activityManager.js';
import { getTime, getDay, getTimestampFromDateString } from '../Helpers/date.js';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import CustomToast from '../Dialog/customToast.js';
import {colors} from '../globalColors.style.js';

export default class ActivityScreen extends Component {
	static navigationOptions = () => ({
    	//header: (headerProps) => <Header {... headerProps} >hihi</Header>,
    	headerTitle: 'Completed Forms',
	});
	constructor(props){
		super(props);
		this.state = {
			activities: [],
			refreshing: false,
			navigation: this.props.navigation,
			firstLoad: true
		}
		this.showDeletedToast = this.showDeletedToast.bind(this);
	}

	componentDidMount(){
		this._fetchActivities();
	}

	async _fetchActivities(){
		console.log(global.loggedInUser);
		const activityResponse = await ActivityManager.getActivity(global.loggedInUser.userId);
		this.processData(activityResponse)
	}

	processData(activityResponse){
		const t_activities = {};
		const component = this;
		//console.log('ActivityResponse: ', activityResponse);
		for(var formName of Object.keys(activityResponse)){
			//console.log('activityResponse[formName]: ', activityResponse[formName]);
			
			const timeAndData = Object.keys(activityResponse[formName]);
			//console.log('timeAndData: ', timeAndData)
			for(var index in timeAndData){
				var timestamp = Number(timeAndData[index]);
				var day = getDay(timestamp);
				var formData = activityResponse[formName][timeAndData[index]].formData;
				var formScope = activityResponse[formName][timeAndData[index]].formScope; 
				var parentPath = activityResponse[formName][timeAndData[index]].parentPath;
				if(t_activities[day] === undefined) t_activities[day] = [];
				t_activities[day].push({
					formName: formName,
					parentPath: parentPath,
					timestamp: timestamp,//new Date(activityResponse[formName].timestamp).toDateString(),//timestampToDate(activityResponse[formName].timestamp).toString(),
					formData: formData,//activityResponse[formName].formData,
					formScope: formScope
				});
			}
		}
		//sort inner dates
		var dayKeys = Object.keys(t_activities); 
		dayKeys.map((time) => (
			t_activities[time].sort((a, b) => (a.timestamp > b.timestamp) ? -1 : 1)
		))

		//sort parent dates
		dayKeys.sort((a, b) => (getTimestampFromDateString(a) > getTimestampFromDateString(b)) ? -1 : 1)

		this.setState({
			activities: t_activities,
			daysArray: dayKeys,
			firstLoad: false
		}, () => {
			component.forceUpdate();
			//console.log('2: ', component.state.daysArray);
		});
	}

	_onRefresh = () => {
		this.setState({
			refreshing: true
		});
		this._fetchActivities().then(()=>{
			this.setState({
				refreshing: false
			});
		})
	}

	showDeletedToast = () =>{
		this.showToast('Form Deleted Successfully');
	}

	_pressActivity(activity){
		//console.log('Pressed activity: ', activity);
		this.state.navigation.navigate('Activity',{ 
			refresh: this._onRefresh,
			activity: activity,
			showDeletedToast: this.showDeletedToast
		});
	}


	render(){
		const component = this;
		return(
			<View style={styles.container}>
				<ScrollView 
					contentContainerStyle={styles.scrollView}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh} />
					}>
				{
					Object.keys(this.state.activities).length > 0 ?
						this.state.daysArray.map((day, index) =>(
							<View key={index} style={styles.dayRow}>
								<Text style={styles.dayText}>{day}</Text>
								<View
								  style={{
								    borderBottomColor: '#96A3B7',
								    borderBottomWidth: 1,
								  }}
								/>
								{
									component.state.activities[day].map((activity, index2)=>(
											<TouchableOpacity 
													key={index.toString() + index2.toString()} 
													style={styles.rowView}
													onPress={()=>{component._pressActivity(activity)}}
													color="#FFF">
												<View style={styles.rowContainer}>
													<View>
														<Text style={styles.detail}>{activity.formName.replace(/_/g,' ')}</Text>
													</View>
													<View>
														<Text style={styles.detail}>{getTime(activity.timestamp)}</Text>
													</View>
												</View>
											</TouchableOpacity>

									))
								}
							</View>
							
						))
						:
						<View style={{flex: 1, alignItems: 'center', justifyContent: 'center', flexDirection:'column'}}>
						{
							!this.state.firstLoad && <Text style={{fontSize: 30, fontFamily: 'Roboto Light', marginTop: 10}}>
								No Activity
							</Text>
						}
						</View>
				}
				</ScrollView>
				<CustomToast 
					setCustomToast={toast => this.showToast = toast}/>
			</View>
		);
	}
}

const styles=StyleSheet.create({
	container: {
		flex: 1,
	},
	// scrollView: {
	// 	alignItems: 'center',
	// 	marginTop: 10,
	// 	paddingBottom: 40,
	// 	margin: 10
	// },
	dayRow: {
		width: '100%',
		padding: 5
	},
	dayText: {
		fontFamily: 'Roboto Light',
		fontSize: 20,
		color: colors.gray2
	},
	rowView: {
		padding: 10,
		marginTop: 7,
		marginBottom: 5,
		borderRadius: 10,
		width: '100%',
		backgroundColor: colors.blue
	},
	rowContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	detail: {
		fontSize: 18,
		color: '#FFF',
		fontFamily: "Roboto Light",
	}
});
