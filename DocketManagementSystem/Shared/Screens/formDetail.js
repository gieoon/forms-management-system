import React, {Component} from 'react';
import {StyleSheet, TouchableHighlight, Image, Text, TextInput, View, Button, ScrollView, BackHandler, RefreshControl, TouchableOpacity} from 'react-native';
//import DatePicker from 'react-native-datepicker'
//https://www.npmjs.com/package/react-native-datepicker

//Local
import { saveFormData } from '../Forms/forms_DB.js';
import { getCleanDate } from '../Helpers/date.js';
import {renderSwitch} from '../FormRendering/formRendering.js';

export default class FormDetail extends Component {
	static navigationOptions = () => ({
    	//header: (headerProps) => <Header {... headerProps} >hihi</Header>,
    	headerTitle: 'New Form'
	});

	constructor(props){
		super(props);
		this.state = {
			form: this.props.navigation.getParam('form'),
			formType: this.props.navigation.getParam('formType'),
			signInBtnOpacity: 1,
			toSave: {},
			listEntry: {}
		}
	}

	async _submit(){
		console.log('submitting form detail state: ', this.state);
		const component = this;
		this.setState({
			signInBtnOpacity: 0.5
		});
		try{
			await saveFormData(
				global.loggedInUser.userId,
				this.state.form.formTitle, 
				this.state.formType,
				this.state.form.parentPath,
				this.state.toSave);
			this.setState({
				signInBtnOpacity: 1
			}, ()=>{
				component.props.navigation.getParam('showSuccessToast')();
				component.props.navigation.goBack(null);
				
			});
		} 
		catch(err){
			console.log("Error Submitting Form: ", err);
		}
	}

	addToFormList(uniqueKey){
		const obj = this.state.listEntry[uniqueKey];
		obj.listLength.push(
			obj.listLength.length
		);
		this.forceUpdate();
		console.log("added to list: ", this.state.listEntry[uniqueKey].listLength);
		//console.log('this.state.listEntry[uniqueKey].ref: ', this.state.listEntry[uniqueKey].ref);
		setTimeout(()=>{
			this.state.listEntry[uniqueKey].ref.current.snapToItem(
				this.state.listEntry[uniqueKey].listLength.length - 1,
				animated=true,
				fireCallback=true);
		}, 150);
		
	}

	removeFromFormList(uniqueKey){
		if(this.state.listEntry[uniqueKey].listLength.length > 1){
			this.state.listEntry[uniqueKey].listLength.splice(
				this.state.listEntry[uniqueKey].ref.current.currentIndex, 1
			); //.pop()
			this.forceUpdate();			

			console.log("removed from list: ", this.state.listEntry[uniqueKey].listLength);
			
			setTimeout(()=>{
				//this.state.listEntry[uniqueKey].ref.current.snapToPrev(animated=true,fireCallback=true);
				
				//causes issues because of splicing, need to update the relevant index only
				if(this.state.listEntry[uniqueKey].ref.current.currentIndex !== 0){
					console.log('snapping to index: ', this.state.listEntry[uniqueKey].ref.current.currentIndex)
					this.state.listEntry[uniqueKey].ref.current.snapToItem (
						this.state.listEntry[uniqueKey].ref.current.currentIndex, 
						animated = true, 
						fireCallback = true
					);

				}
			}, 150)
			
		}
	}

	render(){
		const component = this;
		return(
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.title}>{this.state.form.formTitle.replace(/_/g,' ')}</Text>
					<Text style={styles.date}>Last Modifed: {
						getCleanDate(this.state.form.formData.modifiedDate)
					}</Text>
				</View>

				<ScrollView 
					//bounces={false}
					contentContainerStyle={styles.scrollView}>
					{
						this.state.form.formData.data.map((data, index) => (
							<View key={index} style={styles.formContainer}>
							{
								renderSwitch(
									Object.keys(data)[0], 
									data[Object.keys(data)[0]], 
									component,
									null,
									listIndex=0,
									formIndex=index,
									listTitle='',
									isFormDetail=true
								)
							}
							</View>
						))
					}
				</ScrollView>
			</View>
		);
	}
}






/*----------------------------------------------*/

// gray '#96A3B7'
//light blue '#68a0cf'
const styles = StyleSheet.create({
	container: {
		width: '100%',
		flex: 1,
	},
	formContainer: {
		alignItems: 'center',
		width: '100%',
		flex: 1
	},
	header: {
		paddingTop: 10,
		backgroundColor: '#347CEF',
		width: '100%',
		paddingLeft: 20,
		marginBottom: 10
	},
	scrollView: {
        backgroundColor: 'transparent',
        paddingLeft:20,
        paddingRight:20,
        paddingBottom: 40,
        alignItems: 'center',
    },
	title: {
		marginBottom: 5,
		fontSize: 24,
		color: '#FFF'
	},
	date: {
		marginBottom: 10,
		fontSize: 14,
		color: '#FFF'
	}
})
