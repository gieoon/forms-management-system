
import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, ScrollView, BackHandler, RefreshControl, TouchableOpacity} from 'react-native';
import {
	HeaderBackButton
} from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/FontAwesome';
//Local
import * as FormsManager from '../Forms/formsManager.js';
import {getUIDisplay} from '../DB/DB_CONSTANTS';
import CustomToast from '../Dialog/customToast.js';
import { colors } from '../globalColors.style.js';

export default class FormsScreen extends Component {
	/*static navigationOptions = ({ navigation }) => {
	    return {
	      	headerTitle: "All Forms",
	  //     	headerStyle: {        
			//   backgroundColor: "transparent"      
			// },
	      	headerLeft: 
	      	<HeaderBackButton 
		      	onPress={() => 
		      		{
		      			//logout user
		      			global.loggedInUser = {};
		      			navigation.goBack(null)
		      		} 
		      	}
	      	/>,
	      	headerLeftTitle: 'Logout',
	      	tabBarIcon: 
		        //const { routeName } = navigation.state;
		        //console.log('routeName: ', routeName);
		        //let iconName;
		        //if (routeName === 'All Forms') {
		        //  iconName = `ios-information-circle${focused ? '' : '-outline'}`;
		        //} else if (routeName === 'Person') {
		        //  iconName = `ios-options${focused ? '' : '-outline'}`;
		        //}

		        //return 
		        <Ionicons name='ios-information-circle' size={25} color='tomato' />
		      //},
		      //showTabIcon: 'true'
		    ,
		    showTabIcon: true
		    // tabBarOptions: {
		    //   activeTintColor: 'tomato',
		    //   inactiveTintColor: 'gray',
		    // },

	      	
	  //     	headerTitleStyle: {
			//     fontWeight: "bold",
			//     color: "#fff",
			//     zIndex: 1,
			//     fontSize: 18,
			//     lineHeight: 23
			// },
			// headerTintColor: "#fff"
		};
    };*/

    static navigationOptions = () => ({
    	//header: (headerProps) => <Header {... headerProps} >hihi</Header>,
    	headerTitle: 'All New Forms',
    	tabBarOptions: {
    		showIcon: true,
    	},
    	tabBarIcon: <Icon name="rocket" size={30} color="tomato" />,
	});
    // static navigationOptions = ({navigation}) => {
    // 	return{
    // 		headerMode: 'none'
    // 	}
    // }

	constructor(props){
		//console.log('props: ', props);
		super(props);

		//this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
		this.state = {
			//forms: this.props.navigation.getParam('forms'), //this is for when using the header AppStackNavigator
			forms: this.props.navigation.dangerouslyGetParent().getParam('forms'),
			requiredForms: {},
			formKeysSorted: [],
			requiredFormKeys: [],
			refreshing: false,
			navigation: this.props.navigation
		}
		this.showSuccessToast = this.showSuccessToast.bind(this);	
	}

	componentDidMount(){
		//console.log('props2: ', this.props);
		const obj = this.sortForms(this.state.forms);
		this.setState({
			formKeysSorted: obj.formKeys,
			requiredForms: obj.requiredForms,
			requiredFormKeys: obj.requiredFormKeys
		})
	}

	_onRefresh = () => {
		this.setState({
			refreshing: true
		});
		this._fetchForms().then(()=>{
			this.setState({
				refreshing: false
			});
		})
	}

	async _fetchForms(){
		
		var forms = await FormsManager.launchForms(
			global.loggedInUser.userId, 
			global.loggedInUser.user
		);

		var obj = this.sortForms(forms);
		
		this.setState({
			forms: forms,
			formKeysSorted: obj.formKeys,
			requiredForms: obj.requiredForms,
			requiredFormKeys : obj.requiredFormKeys
		});

		console.log('Getting forms: ', forms);
	}

	sortForms(forms){
		const formKeys = Object.keys(forms);
		const requiredFormKeys = [];
		const requiredForms = {};

		formKeys.sort((a,b) => (a < b ? -1 : 1));
		formKeys.map(function(formType){
			forms[formType].sort((a,b) => (a.formTitle < b.formTitle ? -1: 1))
			console.log('form: ', forms[formType]);
			for(f of forms[formType]){
				if(f.formData.required){
					if(requiredForms[formType] === undefined) {
						requiredForms[formType] = [];
						requiredFormKeys.push(formType);
					}
					requiredForms[formType].push(f);
				}
			}
		})
		
		

		return {
			formKeys: formKeys,
			requiredForms: requiredForms,
			requiredFormKeys: requiredFormKeys
		}
	}

	_pressButton(formObj){
		//console.log('Button pressed: ', form);
		this.state.navigation.navigate('New Form', { 
			form: formObj.form,
			formType: formObj.formType,
			showSuccessToast: this.showSuccessToast
		});
	}

	showSuccessToast = () =>{
		//console.log('this.showToast: ', this.showToast)
		this.showToast("Form Saved Successfully");
	}
	
	render(){
		return(
			<View style={styles.container}>
				<ScrollView 
					//bounces={false}
					contentContainerStyle={styles.scrollView}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh}
						/>
					}>
				<View style={styles.requiredFormsContainer}>
					<View style={styles.formType}>
						<Text style={styles.sectionText}>Required Forms</Text>
						
					</View>
				{
					//forms that are required and have not been filled out yet
					this.state.requiredFormKeys.map((formType, index) => (
						<View key={index} style={styles.formType}>
							<Text style={styles.formTypeText}>{getUIDisplay(formType)}</Text>
							<View
							  style={{
							    borderBottomColor: '#96A3B7',
							    borderBottomWidth: 1,
							  }}
							/>
							{
								this.state.requiredForms[formType].map((form, index) => (
									<TouchableOpacity 
											key={index} 
											style={form.formData.required ? styles.requiredForm : styles.form}
											onPress={()=>{this._pressButton({
												form: form,
												formType: formType
											})}}
											color="#FFF">
										<Text style={styles.formText}>{form.formTitle.replace(/_/g,' ')}</Text>
									</TouchableOpacity>
								))
							}
						</View>
					))			
				}
				</View>

				<View style={styles.formType}>
						<Text style={styles.sectionText}>All Forms</Text>	
				</View>
				{
					this.state.formKeysSorted.map((formType, index) => (
						<View key={index} style={styles.formType}>
							<Text style={styles.formTypeText}>{getUIDisplay(formType)}</Text>
							<View
							  style={{
							    borderBottomColor: '#96A3B7',
							    borderBottomWidth: 1,
							  }}
							/>
							{
								this.state.forms[formType].map((form, index) => (
									<TouchableOpacity 
											key={index} 
											style={form.formData.required ? styles.requiredForm : styles.form}
											onPress={()=>{this._pressButton({
												form: form,
												formType: formType
											})}}
											color="#FFF">
										<Text style={styles.formText}>{form.formTitle.replace(/_/g,' ')}</Text>
									</TouchableOpacity>
								))
							}
						</View>
					))
					
				}
				</ScrollView>
				<CustomToast 
					//ref={this.customToastRef}
					setCustomToast={toast => this.showToast = toast}/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	requiredFormsContainer : {
		backgroundColor: colors.lightgray,
		width: '100%'
	},
	scrollView: {
		alignItems: 'center',
		justifyContent: 'center',
		//marginTop: 10,
		paddingBottom: 40,
	},
	formType: {
		padding: 5,
		width: '100%'
	},
	sectionText: {
		fontSize: 24,
		color: colors.gray2,
		fontFamily: "Roboto",
		textAlign: 'center',
		paddingTop: 20,
		letterSpacing: 0.25 
	},
	formTypeText: {
		fontSize: 20,
		color: colors.gray2,
		fontFamily: "Roboto Light",
	},
	formText: {
		color: '#FFF',
		fontSize: 18,
		fontFamily: "Roboto Light",
	},
	form: {
		padding: 10,
		marginTop: 7,
		marginBottom: 5,
		borderRadius: 10,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.blue
	},
	requiredForm: {
		padding: 10,
		marginTop: 7,
		marginBottom: 5,
		borderRadius: 10,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: colors.red
	}
})

/*
	headerRight: (
      <Button
        onPress={() => alert('This is a button!')}
        title="Info"
        color="#fff"
     />
*/


////onPress={() => navigation.goBack(null)} />,

