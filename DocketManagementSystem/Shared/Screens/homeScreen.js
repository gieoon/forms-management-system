import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button, Image} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';

//Locals
import SignIn from '../SignIn/signIn.js';

type Props = {};
//export default class App extends Component<Props> {
class HomeScreen extends React.Component { 
  static navigationOptions = ({ navigation }) => {
    return {
      tabBarVisible: false,
    };
  };

  constructor(props){
    super(props)
    this.state = {
      showSignIn: true//false
    }
  }

  _onSignIn() {
    this.setState({
      showSignIn: true
    });
  }

  _signInSuccessful(forms, navigation){
    //console.log('this.props.navigation: ', navigation);
    //console.log('HomeScreen forms: ', forms);
    navigation.navigate('Forms',{ forms: forms });
  }


  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logoImage} source={require('../Resources/Your-Logo-here.png')}></Image>
        <Text style={styles.companyName}>Your Company Name Here</Text>
        <Text style={styles.welcome}>Docket Management System</Text>
        {
          this.state.showSignIn ? 
            <SignIn signInSuccessful={this._signInSuccessful} navigation={this.props.navigation} />
          :
            <Button 
            style={styles.instructions} 
            title='Sign In' 
            onPress={() => {this._onSignIn() }}
            />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    fontFamily: "Roboto Light",
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    fontFamily: "Roboto Light",
  },
  companyName: {
    fontSize: 30,
    textAlign: 'center',
    margin: 15,
    fontWeight: 'bold',
    fontFamily: "Roboto Light",
  }
});

export default HomeScreen;



