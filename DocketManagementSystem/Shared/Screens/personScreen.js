import React, {Component} from 'react';
import {StyleSheet, Text, View, Button, ScrollView, BackHandler, RefreshControl} from 'react-native';
import { Header } from 'react-navigation';
import styles, { sliderWidth, itemWidth } from './personScreen.style.js';
//import styles from './personScreenSliderInner.style.js';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import { colors } from '../globalColors.style.js';
import { getJobAssignmentForUser } from '../JobAssignments/jobAssignments_DB.js';
import {getCleanDate, getTimestampFromDateString} from '../Helpers/date.js';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class PersonScreen extends Component {

 	static navigationOptions = () => ({
    	//header: (headerProps) => <Header {... headerProps} >hihi</Header>,
    	headerTitle: 'Your Assignments' //'Your Details'
	});

	constructor(props){
		super(props);
		this.state = {
			sliderActiveSlide: 1,
			jobs: [],
			assignments: [],
			startingSliderIndex: 0//set to today
		}
	}

	componentDidMount(){
		this._fetchJobAssignments();
	}

	async _fetchJobAssignments(){
		const assignments = await getJobAssignmentForUser(global.loggedInUser.userId);
		//console.log('assignments: ', assignments);
		this.setState({ startingSliderIndex: assignments.length - 1});
		assignments.sort((a, b) => (getTimestampFromDateString(a.StartDate) < getTimestampFromDateString(b.StartDate)) ? -1 : 1)
		
		const currentDate = Date.now();
		for(var i=0;i<assignments.length;i++){
			if(getTimestampFromDateString(assignments[i].AssignmentEndDate) > currentDate){
				this.setState({ 
					startingSliderIndex: i,
					assignments: assignments
				});
				break;
			}
		}
	}

	get gradient () {
        return (
            <LinearGradient
              colors={[colors.background1, colors.background2]}
              startPoint={{ x: 1, y: 0 }}
              endPoint={{ x: 0, y: 1 }}
              style={styles.gradient}
            />
        );
    }

    renderMoreDetails() {
		<View>
			<View style={userStyles.nameView}>
				<Text style={userStyles.name}>
					{global.loggedInUser.user.FirstName + ' '}
					{global.loggedInUser.user.LastName}
				</Text>
			</View>
			
			<View style={userStyles.horizontalRule}/>

			<View style={userStyles.rowView}>
				<View>
					<Text style={userStyles.header}>Roles:</Text>
				</View>
				<View>
				{
					global.loggedInUser.user.UserRole.map((userRole, index) => (
						<Text key={index} style={[userStyles.details, userStyles.centered]}>{userRole.replace(/_/g, ' ')}</Text>
					))
				}
				</View>
			</View>
			<View style={userStyles.horizontalRule}/>

			<View style={userStyles.rowView}>
				<View>
					<Text style={userStyles.header}>Occupations:</Text>
				</View>
				<View>
				{
					global.loggedInUser.user.Occupation.map((occupation, index) => (
						<Text key={index} style={userStyles.details}>{occupation.replace(/_/g, ' ')}</Text>
					))
				}
				</View>
			</View>
			<View style={userStyles.horizontalRule}/>

			<View style={userStyles.rowView}>
				<View>
					<Text style={userStyles.header}>Sites:</Text>
				</View>
				<View>
				{
					global.loggedInUser.user.Site.map((site, index) => (
						<Text key={index} style={userStyles.details}>{site.replace(/_/g, ' ')}</Text>
					))
				}
				</View>
			</View>
			<View style={userStyles.horizontalRule}/>

			<View style={userStyles.rowView}>
				<View>
					<Text style={userStyles.header}>Tools:</Text>
				</View>
				<View>
				{
					global.loggedInUser.user.Tool.map((tool, index) => (
						<Text key={index} style={userStyles.details}>{tool.replace(/_/g, ' ')}</Text>
					))
				}
				</View>
			</View>
		</View>
    }

    renderPersonDetailsWithParallax = (assignment) => {
    	const item = assignment.item;
    	//console.log('item: ', item);
    	
    	return 	<View style={styles.parent}>
    				<View style={styles.statusContainer}>
    					<Text style={[styles.statusText, 
    						item.Status === 'UPCOMING' 
    						? styles.yellow 
    						: item.Status === 'ONGOING' //CURRENT 
    						? styles.green 
    						: item.Status === 'CANCELLED'
    						? styles.red 
    						: item.Status === 'COMPLETED'
    						? styles.orange
    						: styles.none]}>
    						{item.Status}
    					</Text>
    				</View>
    	 			<View style={[styles.textContainer, {paddingBottom: 0}]}>
						<Text style={styles.header}>Starting</Text>
						<Text style={styles.header}>Ending</Text>
					</View>
					<View style={[styles.textContainer, {paddingTop: 0}]}>
						<Text style={styles.subtitle}>{item.StartDate}</Text>
						<Text style={styles.header}> - </Text>
						{/*<Ionicons name='ios-arrow-round-forward' size={20} color='black'/>*/}
						<Text style={styles.subtitle}>{item.AssignmentEndDate}</Text>
					</View>

    				<Text
		             	style={[styles.title ]} //styles.titleEven
		            	numberOfLines={2}
		            	>
		                { item.Title.toUpperCase() }
		            </Text>
		            <View style={styles.shadow} />
					
					<View style={styles.textContainer}>
						<Text style={styles.header}>Sites:</Text>
						<View style={styles.verticalContainer}>
						{
							item.Sites.map((s, index) => (
								<Text key={index} style={styles.subtitle}>{s}</Text>
							))
						}
						</View>
					</View>
					<View style={userStyles.horizontalRule}/>
					<View style={styles.textContainer}>
						<Text style={styles.header}>Occupations:</Text>
						<View style={styles.verticalContainer}>
						{
							item.Occupations.map((o, index) => (
								<Text key={index} style={styles.subtitle}>{o}</Text>
							))
						}
						</View>
					</View>
					<View style={userStyles.horizontalRule}/>
					<View style={styles.textContainer}>
						<Text>Roles:</Text>
						<View style={styles.verticalContainer}>
						{
							item.UserRoles.map((o, index) => (
								<Text key={index} style={styles.subtitle}>{o}</Text>
							))
						}
						</View>
					</View>
					<View style={userStyles.horizontalRule}/>

					<View style={styles.textContainer}>
						<Text>Required Tools:</Text>
						<View style={styles.verticalContainer}>
						{
							item.Tools.map((t, index) => (
								<Text key={index} style={styles.subtitle}>{t}</Text>
							))
						}
						</View>
					</View>
					<View style={userStyles.horizontalRule}/>

					<View style={styles.textContainer}>
    					<Text style={styles.header}>Created on:</Text>
						<Text style={styles.subtitle}>{getCleanDate(item.timestamp)}</Text>
					</View>
					

    		   	</View>    
    }

	render(){
		const { sliderActiveSlide } = this.state;

		return(
			<View style={userStyles.container}>
				
				<Carousel
					ref={c => this.sliderRef = c}
					data={this.state.assignments}
					renderItem={this.renderPersonDetailsWithParallax}
					sliderWidth={sliderWidth}
					itemWidth={itemWidth}
					hasParallaxImages={true}
					firstItem={this.state.startingSliderIndex}
					inactiveSlideScale={0.94}
					inactiveSlideOpacity={0.7}
					containerCustomStyle={styles.slider}
					contentContainerCustomStyle={styles.sliderContentContainer}
					loop={true}
					loopClonesPerSide={2}
					autoplay={false}
					//autoplayDelay={500}
					//autoplayInterval={3000}
					onSnapToItem={(index) => this.setState({ sliderActiveSlide: index })}
				/>
				<Pagination
					dotsLength={this.state.assignments.length}
					activeDotIndex={sliderActiveSlide}
					containerStyle={styles.paginationContainer}
					dotColor={'rgba(0,0,0, 0.92)'}
					dotStyle={styles.paginationDot}
					inactiveDotColor={colors.black}
					inactiveDotOpacity={0.4}
					inactiveDotScale={0.6}
					carouselRef={this.sliderRef}
					tappableDots={!!this.sliderRef}
				/>
			</View>
		);
	}
}

/*
<ScrollView contentContainerStyle={styles.scrollView}>
					
</ScrollView>
*/

// const styles = StyleSheet.create({
// 	container: {
// 		paddingVertical: 30
// 	},
// 	rowView: {
// 		alignItems: 'center'
// 	}

// });

const userStyles = StyleSheet.create({
	container: {
		paddingVertical: 30,
		// marginTop: 20,
		// flex: 1,
	},
	rowView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 10,
		marginLeft: 15,
		marginRight: 15
	},
	nameView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
	},
	scrollView: {
		backgroundColor: 'transparent',
        paddingBottom: 40,
	},
	name: {
		fontSize: 32,
		textAlign: 'center',
		margin: 4,
		fontFamily: "Roboto",
	},
	headerContainer: {
		backgroundColor: '#96A3B7',
		paddingTop: 5,
		paddingBottom: 10
	},
	title: {
		textAlign: 'center',
		marginBottom: 5,
		fontSize: 20,
	},
	header: {
		fontSize: 20,
		color: '#96A3B7',
		marginTop: 15,
		fontFamily: "Roboto Light",
	},
	details: {
		fontSize: 18,
		margin: 5,
		marginTop: 15,
		fontFamily: "Roboto Light",
	},
	horizontalRule: {
	    borderBottomColor: '#96A3B7',
	    borderBottomWidth: 1,
	    marginLeft: 15,
	    marginRight: 15
	},
})


// 				<View style={styles.container}>
// 				{/*<View style={styles.headerContainer}>
// 					<Text style={[styles.title, styles.centered]}>Your Details</Text>
// 				</View>*/}
// 				<ScrollView contentContainerStyle={styles.scrollView}>
// 					<View style={styles.nameView}>
// 						<Text style={styles.name}>
// 							{global.loggedInUser.user.FirstName + ' '}
// 							{global.loggedInUser.user.LastName}
// 						</Text>
// 					</View>
// 					<View style={styles.rowView}>
// 						<View>
// 							<Text style={styles.header}>Start Date:</Text>
// 						</View>
// 						<View>
// 							<Text style={styles.details}>{global.loggedInUser.user.StartDate}</Text>
// 						</View>
// 					</View>
// 					<View style={styles.horizontalRule}/>

// 					<View style={styles.rowView}>
// 						<View>
// 							<Text style={styles.header}>Roles:</Text>
// 						</View>
// 						<View>
// 						{
// 							global.loggedInUser.user.UserRole.map((userRole, index) => (
// 								<Text key={index} style={[styles.details, styles.centered]}>{userRole.replace(/_/g, ' ')}</Text>
// 							))
// 						}
// 						</View>
// 					</View>
// 					<View style={styles.horizontalRule}/>

// 					<View style={styles.rowView}>
// 						<View>
// 							<Text style={styles.header}>Occupations:</Text>
// 						</View>
// 						<View>
// 						{
// 							global.loggedInUser.user.Occupation.map((occupation, index) => (
// 								<Text key={index} style={styles.details}>{occupation.replace(/_/g, ' ')}</Text>
// 							))
// 						}
// 						</View>
// 					</View>
// 					<View style={styles.horizontalRule}/>

// 					<View style={styles.rowView}>
// 						<View>
// 							<Text style={styles.header}>Sites:</Text>
// 						</View>
// 						<View>
// 						{
// 							global.loggedInUser.user.Site.map((site, index) => (
// 								<Text key={index} style={styles.details}>{site.replace(/_/g, ' ')}</Text>
// 							))
// 						}
// 						</View>
// 					</View>
// 					<View style={styles.horizontalRule}/>

// 					<View style={styles.rowView}>
// 						<View>
// 							<Text style={styles.header}>Tools:</Text>
// 						</View>
// 						<View>
// 						{
// 							global.loggedInUser.user.Tool.map((tool, index) => (
// 								<Text key={index} style={styles.details}>{tool.replace(/_/g, ' ')}</Text>
// 							))
// 						}
// 						</View>
// 					</View>

// 				</ScrollView>
// 			</View>
