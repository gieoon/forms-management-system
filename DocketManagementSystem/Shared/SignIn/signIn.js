
import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Image, TouchableHighlight} from 'react-native';

//local imports
import * as FormsManager from '../Forms/formsManager.js';
import * as User from '../Users/user.js';
import {colors} from '../globalColors.style.js';

class SignIn extends Component {
	constructor(props){
		super(props);
		this.state = {
			username: '',
			password: '',
			firstName: '',
			lastName: '',
			b_loginFailed: false,
			pressedSignIn: false,
			signInBtnOpacity: 1
		}
	}

	async _signIn(){
		if(!this.state.pressedSignIn){
			this.setState({
				pressedSignIn: true,
				signInBtnOpacity: 0.5
			})
			const loginObj = await User.login(this.state.firstName,this.state.lastName);
			if(Object.keys(loginObj).length > 0){
				console.log('Login Successful: ', loginObj.loginTime, ' User: ', loginObj.user);
				const component = this;
				this.setState({
					username: '',
					password: '',
					firstName: '',
					lastName: '',
					b_loginFailed: false,
					pressedSignIn: false,
					signInBtnOpacity: 1
				});
				global.loggedInUser = loginObj;
				this.launchSignInForms(loginObj.userId, loginObj.user);
			}
			else {
				console.log('Login Failed: ', loginObj);
				this.setState({
					b_loginFailed: true,
					pressedSignIn: false,
					signInBtnOpacity: 1
				});
			}
		}
	}

	async launchSignInForms(userId, user) {
		const forms = await FormsManager.launchForms(userId, user);
		//console.log('this.props: ',this.props.signInSuccessful)
		//console.log('forms: ', forms);
		this.props.signInSuccessful(forms, this.props.navigation);
	}

	render(){
		return(
			<View>
				{
					this.state.b_loginFailed && 
					<Text style={styles.errLabel}>There was an issue logging in</Text>	
				}
				<Text style={styles.textLabel}>First Name</Text>
				<TextInput 
					style={styles.inputArea}
					value={this.state.firstName}
					onChangeText={(firstName) => this.setState({firstName})}
				/>
				<Text style={styles.textLabel}>Last Name</Text>
				<TextInput
					style={styles.inputArea} 
					value={this.state.lastName}
					onChangeText={(lastName) => this.setState({lastName})}
				/>
				<TouchableHighlight 
					style={[styles.button, {opacity: this.state.signInBtnOpacity}]}
					onPress={() => this._signIn()}>
					<Text style={styles.btnLabel}>Sign In</Text>
				</TouchableHighlight>
			</View>
		)
	}
}



const styles = StyleSheet.create({
	inputArea: {
		height: 40,
		width: 250,
		borderColor: 'gray',
		borderWidth: 1,
		textAlign: 'center',
		fontSize: 20,
		fontWeight: 'bold',
		fontFamily: "Roboto Light",
	},
	textLabel: {
		textAlign:'center',
		marginTop:10,
		marginBottom:2,
		fontSize: 18,
		fontFamily: "Roboto Light",

	},
	errLabel: {
		textAlign:'center',
		marginTop:10,
		marginBottom:5,
		fontSize:12,
		color:'#FF7F50'
	},
	btnLabel: {
		color:'#fff',
      	textAlign:'center',
      	fontSize: 18
	},
	button: {
    	marginRight:40,
	    marginLeft:40,
	    marginTop:20,
	    paddingTop:10,
	    paddingBottom:10,
	    backgroundColor: colors.blue,
	    borderRadius:10,
	    borderWidth: 1,
	    borderColor: '#fff'
  },
});

export default SignIn;

