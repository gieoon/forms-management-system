
import firebase from 'react-native-firebase';
import * as DB_CONSTANTS from '../DB/DB_CONSTANTS.js';



export async function authenticateUser(firstName, lastName){
	var sameFirstNames = firebase.firestore()
		.collection(DB_CONSTANTS.FIRESTORE_ENDPOINT_USERS)
		.where('FirstName', '==', firstName);
	
	return sameFirstNames
		.get()
		.then(function(userSnapshot){
			for (var i in userSnapshot.docs){
				const doc = userSnapshot.docs[i];
				//console.log('doc: ', doc.data());
				if(doc.get('LastName') === lastName){
					return {
						loginTime: Date.now(),
						userId: doc.id,
						user: doc.data()
					}
				}	
			}
			return {};
		}).catch(function(err){
			console.log("Error getting document: ", err);
		});

}
