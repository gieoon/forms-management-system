export const colors = {
    black: '#1a1917',
    gray: '#888888',
    gray2: '#96A3B7',
    lightgray: '#e5e5e5',//#dbd9d9',
    red: '#ED4245',
    blue: '#347CEF',
    background1: '#FFF',//'#B721FF',
    background2: '#FFF'//'#21D4FD'
};