/**
 * @format
 */

 /*
	Need to suppress this warning because am using an older version of react-native, in the latest it will be removed
 */
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

import React, {Component} from 'react';

import CustomToast from './Shared/Dialog/customToast.js';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName, toast as toast} from './app.json';


AppRegistry.registerComponent(appName, () => App);
//AppRegistry.registerComponent(toast, () => <CustomToast />);
//AppRegistry.registerComponent(appName, () => FormsScreen);


